import { Flex, Spacer, Center, Text,Square,Box, ChakraProvider, Spinner, SimpleGrid, WrapItem, Wrap, Tooltip } from '@chakra-ui/react'
import { AddIcon } from '@chakra-ui/icons'
import { useEffect, useState } from 'react';
import ProjectLandingPage from '../ProjectLandingPage/ProjectLandingPage';

import { useNavigate } from "react-router-dom"

import "./ProjectList.css"
import AddProjectModel from './AddProjectModel';

import axios from "axios"

import { useToast } from '@chakra-ui/react'


const ProjectList = ()=>{

    const [ showAddProjectFormModel, setShowAddProjectFormModel ] = useState(false)

    const [ reload, setReload ] = useState(false)

    const [projectList, setProjectList] = useState([])

    const [loading,setLoading] = useState(false)

    const toast = useToast()

    const toast1 = (data,status)=>{
        console.log("data,status",data,status);
       return toast({
            title: status,
            description: data,
            status: status,
            position:"bottom-right",
            duration: 5000,
            isClosable: true,
          })
    }

    useEffect(()=>{
       fetch(`https://jsonplaceholder.typicode.com/todos/1`)
        .then(response => response.json())
        .then(json => console.log(json))
        .catch(err=>{
            console.log(err);
        })

        // return(()=>{
        //   alert("hello")
        // })

    },[reload])

    useEffect(()=>{
        setLoading(true)
         axios.post("http://localhost:8081/project/get-all-projects")
        .then(data=>(setProjectList(data.data),setLoading(false)))
        .catch(err=>{
            console.log(err);
            let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
        })
        return(()=>{
            setReload(false)
        })
    },[reload] )

    console.log(projectList);

    let Project = {
        projectName:"Project 1",
        projectId : 1
    }

    const projects = [
        {
            projectName:"project 1",
            projectId:1
        },
        {
            projectName:"project 2",
            projectId:2
        },
        {
            projectName:"project 3",
            projectId:3
        },
        {
            projectName:"project 4",
            projectId:4
        },
        {
            projectName:"project 5",
            projectId:5
        }
    ]

    let history =  useNavigate()

    const addProject = ()=>{
        console.log("addProject");
        setShowAddProjectFormModel(true)
    }

    const submitProject = ()=>{
        console.log("submitProject");
        setReload(true)
    }

    const closeForm = ()=>{
        console.log("closeForm");
        setShowAddProjectFormModel(false)
    }

    const projectLandingPageHandler = ()=>{
        console.log("projectLandingPageHandler",Project.projectId);
        // history("/projectLandingPage")
        history(`/project/${Project.projectId}`)
    }

    const projectHandler = (data)=>{
        console.log("projectIdHandler",data );
        history(`/project/${data}`)
    }

    return(
    <ChakraProvider>

        { loading? <Center> <Spinner size='xl' m={"120px"} /> </Center> : <> <Box  w="100%" p={4}>
           <Flex>
              <Spacer/>
              <Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" fontWeight={"bold"}> Total Project Count: {projectList.length} </Text>
          </Flex>
        </Box>
  <Center>
  <Wrap ml={"10px"} mr={"10px"} justify='center' color="white" className="projectListingWrap"  >   
  <WrapItem cursor={"pointer"} onClick={addProject}  >
    <Center w='300px' h='80px' fontSize='20px' bg='' color={"black"} border="2px solid black" mb={"10px"} borderRadius={"20px"} >
    < Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" > <AddIcon  mb="5px" mr="7px" /> Create Project </Text>
    </Center>
    </WrapItem>

    { projectList.map(data=>{
         return<WrapItem key={data._id} cursor={"pointer"} onClick={projectHandler.bind(null, data._id) } >
         <Center w='300px' h='80px'  bg='#4056A1' borderRadius={"20px"} mb={"10px"} border="2px solid black" >
         <Tooltip label={data.project_description} >
 <Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" > {data.project_name} </Text> 
 </Tooltip>
 </Center>
     </WrapItem>
    }) }

</Wrap>
    </Center> 
    </>
     }


    {/* <WrapItem cursor={"pointer"} onClick={projectLandingHandler} > */}
    {/* <WrapItem cursor={"pointer"} onClick={projectLandingPageHandler} >
    <Center w='300px' h='80px' bg='red.200' borderRadius={"20px"} mb={"10px"} >
    <Text> "Project.projectName" </Text> 
    </Center>
    </WrapItem> */}

    {/* { projects.map( data =>{
        
        return<WrapItem key={data.projectId} cursor={"pointer"} onClick={projectIdHandler.bind(null, data.projectId) } >
            <Center w='300px' h='80px'  bg='#4056A1' borderRadius={"20px"} mb={"10px"} border="2px solid black" >
            <Tooltip label={description} >
    <Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" > {data.projectName} </Text> 
    </Tooltip>
    </Center>
        </WrapItem>
    } ) } */}

 {/* { projectList.map(data=>{
         return<WrapItem key={data._id} cursor={"pointer"} onClick={projectIdHandler.bind(null, data._id) } >
         <Center w='300px' h='80px'  bg='#4056A1' borderRadius={"20px"} mb={"10px"} border="2px solid black" >
         <Tooltip label={data.project_description} >
 <Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" > {data.project_name} </Text> 
 </Tooltip>
 </Center>
     </WrapItem>
    }) }  */}


    {/* <Tooltip label={description} placement='top' >
    <WrapItem cursor={"pointer"} >
    <Center w='300px' h='80px' bg='red.200' borderRadius={"20px"}>
    <Text> Project 2 </Text> 
    </Center>
    </WrapItem>
    </Tooltip>
    <WrapItem cursor={"pointer"} >
    <Center w='300px' h='80px' bg='red.200' borderRadius={"20px"}>
    <Text> Project 3 </Text> 
    </Center>
    </WrapItem>
    <WrapItem cursor={"pointer"} >
    <Center w='300px' h='80px' bg='red.200' borderRadius={"20px"}>
    <Text> Project 4 </Text> 
    </Center>
    </WrapItem> */}

    

    {/* { projectLanding && <ProjectLandingPage/>} */}

{/* <SimpleGrid  spacing={10} p={"40px"}>
  <Box bg='tomato' height='80px' onClick={projectHandler} > <Center h='80px' > Project-1 </Center>  </Box>
  <Box bg='tomato' height='80px'> <Center h='80px' > project-2 </Center> </Box>
  <Box bg='tomato' height='80px'> <Center h='80px' > project-3 </Center></Box>
  <Box bg='tomato' height='80px'> <Center h='80px' > project-4 </Center></Box>
  <Box bg='tomato' height='80px'>  <Center h='80px'> project-5 </Center></Box>
</SimpleGrid> */}

{ showAddProjectFormModel && <AddProjectModel showForm={true} submitProject={submitProject} closeShowAddProjectFormModel={closeForm} />}

</ChakraProvider>
    
        
    )

}


export default ProjectList;