import { ChakraProvider, FormControl, FormLabel, Input, Center } from "@chakra-ui/react"
import { useRef, useState } from "react"

import {Modal, Button} from "react-bootstrap"

import axios from "axios"

const AddProjectModel = (props)=>{

  const [projectName, setProjectName] = useState( props.projectDetails?.project_name || "")

  const [projectDescription, setProjectDescription] = useState(props.projectDetails?.project_description ||"")

  const [disabled, setDisabled ] = useState(props.projectDetails? false: true )

  const inputRef = useRef()

  const projectNameChange = (e)=>{
    setProjectName(e.target.value)
    if(e.target.value.length >= 3 && projectDescription.length >= 2 ){
      setDisabled(false)
    } else{
      setDisabled(true)
    }
  }

  const projectDescriptionChange = (e)=>{
    setProjectDescription(e.target.value)
    if(e.target.value.length >= 2 && projectName.length >= 3 ) {
      setDisabled(false)
    } else{
      setDisabled(true)
    }
  }

  const submitProjectDetails = async ()=>{
    const createProjectBody = {
      id:props.projectDetails?._id || "",
      project_name:projectName,
      project_description:projectDescription
    }
    const createProject = await axios.post("http://localhost:8081/project/create-project",createProjectBody)
    .then(data=>data)
    .catch(err=>{
      console.log("err",err);
    })
    console.log(createProject);
    if(createProject.status === 200){
      props.submitProject()
      props.closeShowAddProjectFormModel()
    }
    // props.submitProject()
    // props.closeShowAddProjectFormModel()
  }

    const handleClose = ()=>{
        console.log("handleClose");
        props.closeShowAddProjectFormModel()
    }

    // console.log("props",props.projectDetails._id);

    return(
        <ChakraProvider>
            <Modal
             onShow={() => {
            inputRef.current.focus();
          }}
            show={props.showForm}
            onHide={handleClose}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter " >
          Add Project
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
          <Center>
      <FormControl width={"600px"}  p={"10px"}>
        <form>
            <FormLabel htmlFor="projectName">Project Name:</FormLabel>
            <Input
              id="projectName"
              type="text"
              mb={"20px"}
              autoComplete="off"
              isRequired
              value={projectName}
              onChange={projectNameChange}
              ref={inputRef}
            />
            <FormLabel htmlFor="projectDescription">Project Description:</FormLabel>
            <Input
              id="projectDescription"
              type="text"
              placeholder="Please provide the Project Description"
              mb={"20px"}
              autoComplete="off"
              isRequired
              value={projectDescription}
              onChange={projectDescriptionChange}
            />
            <Button type="submit" onClick={submitProjectDetails} disabled={disabled} >
              Submit
            </Button>
            </form>
          </FormControl>
          </Center>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose} >Close</Button>
      </Modal.Footer>
    </Modal>
        </ChakraProvider>
    )
}


export default AddProjectModel