import { ChakraProvider } from "@chakra-ui/react"
import TestCaseForm from "../TestCaseForm/TestCaseForm"

const AddTestCase = ()=>{

    return(
        <ChakraProvider>
            <TestCaseForm></TestCaseForm>
        </ChakraProvider>
    )
}


export default AddTestCase