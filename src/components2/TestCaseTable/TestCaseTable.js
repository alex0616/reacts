import * as React from "react";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Tfoot,
  chakra
} from "@chakra-ui/react";
import {
  TriangleDownIcon,
  TriangleUpIcon
} from "@chakra-ui/icons";
import { useTable, useSortBy, Column } from "react-table";

const TestCaseTable = ()=>{
    return(
        <Table size='sm'  >
  <Thead>
    <Tr>
      <Th>To convert</Th>
      <Th>into</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>
      <Th >multiply by</Th>

    </Tr>
  </Thead>
  <Tbody>
    <Tr>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
      <Td >25.4</Td>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
    </Tr>
    <Tr>
    <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
      <Td >25.4</Td>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
    </Tr>
    <Tr>
      <Td>yards</Td>
      <Td>metres (m)</Td>
      <Td >0.91444</Td>
      <Td>yards</Td>
      <Td>metres (m)</Td>
      <Td >0.91444</Td>
      <Td >25.4</Td>
      <Td>inches</Td>
      <Td>millimetres (mm)</Td>
      <Td >25.4</Td>
    </Tr>
  </Tbody>
</Table>
    )
}

export default TestCaseTable