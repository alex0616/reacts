import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Center,
  Button,
  form,
  ChakraProvider,
} from "@chakra-ui/react";
import { useState } from "react";

const ProjectForm = () => {
  const [projectName, setProjectName] = useState("");
  const [projectShortName, setProjectShortName] = useState("");

  const projectNameHandler = (name) => {
    setProjectName(name.target.value);
  };

  const projectShortNameHandler = (shortName) => {
    setProjectShortName(shortName.target.value);
  };

  const createProjectHandler = (event) => {
    event.preventDefault();
    console.log("createProjectHandler", projectName, projectShortName);

    setProjectName("");
    setProjectShortName("");
  };

  return (
    <ChakraProvider>
      <form onSubmit={createProjectHandler}>
        <Center>
          <FormControl width={"600px"} mt={"30px"} p={"10px"}>
            <FormLabel htmlFor="projectName">Project Name:</FormLabel>
            <Input
              id="projectName"
              type="text"
              mb={"20px"}
              autoComplete="off"
              value={projectName}
              onChange={projectNameHandler}
              isRequired
            />
            {/* <FormHelperText>We'll never share your email.</FormHelperText> */}
            <FormLabel htmlFor="projectShortForm">Project shortForm:</FormLabel>
            <Input
              id="projectShortForm"
              type="text"
              placeholder="Please provide the shortform name"
              mb={"20px"}
              value={projectShortName}
              onChange={projectShortNameHandler}
              isRequired
            />
            <Button type="submit" colorScheme="blue">
              Submit
            </Button>
          </FormControl>
        </Center>
      </form>
    </ChakraProvider>
  );
};

export default ProjectForm;
