import { ChakraProvider, GridItem, SimpleGrid, FormLabel, Input, Text, Flex, Heading, Center, HStack, Grid, Wrap,WrapItem ,Box, Icon, Image, AspectRatio, Spacer, Button, Spinner, OrderedList, ListItem, Tooltip } from "@chakra-ui/react"

import {useEffect, useState} from "react"
import "./ViewBugReport.css"

import ImageModel from "./ImageModel"
import { useLocation, useNavigate, useParams } from "react-router-dom"

import {TiTick} from "react-icons/ti"

import {FcApproval} from "react-icons/fc"

import {AiTwotoneAlert, AiOutlineAlert, AiFillAlert} from "react-icons/ai"

import {GoAlert} from "react-icons/go"

import {IoAlertOutline} from "react-icons/io5" 

import { useToast } from '@chakra-ui/react'

import axios from "axios"

const ViewBugReport = ()=>{

    const [ viewAttachmentModel, setViewAttachmentModel ] = useState(false)

    const history = useNavigate()

    const location = useLocation()

    const {bugReportId} = useParams()

    // let bugReportData = location.state.data

    // console.log("location",bugReportData);

    const toast = useToast()

    const [ image, setImage ] = useState([])

    const [ attachmentData, setAttachmentData ] = useState()

    const [loading, setLoading] = useState(true)

    const [singleBugReportDataFromAPI, setSingleBugReportDataFromAPI] = useState([])

    const [step,setStep] = useState([])

    const toast1 = (data,status)=>{
        console.log("data,status",data,status);
       return toast({
            title: status,
            description: data,
            status: status,
            position:"bottom-right",
            duration: 5000,
            isClosable: true,
          })
    }

    const editBugReport = ()=>{
        console.log("editBugReport");
        history(`/editBugReport/${singleBugReportDataFromAPI.project}/${singleBugReportDataFromAPI._id}`,{state:{data:singleBugReportDataFromAPI}})
    }

    const deleteBugReport = async()=>{
        console.log("deleteBugReport",singleBugReportDataFromAPI._id);

        const deleteBugReport = await axios.delete(`http://localhost:8081/bugreport/delete/${singleBugReportDataFromAPI._id}`)
        .then(data=>(console.log(data),data))
        .catch(err=>{
            console.log("err",err.response);
            let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
        })

        if(deleteBugReport.status === 200){
            console.log(deleteBugReport);
            history(`/bugReport/${singleBugReportDataFromAPI.project}`)
            toast1(deleteBugReport.data,"success")
        }
    }

    
    useEffect( async ()=>{
                
        const getBugReport = await axios.get(`http://localhost:8081/bugreport/bug-report/${bugReportId}`)
        .then(data=>(setSingleBugReportDataFromAPI(data.data), setLoading(false), console.log("data from API",data.data),data))
        .catch(err=>{
            console.log("err");
        })
        
        if(getBugReport.status===200){
            console.log(getBugReport.data);
            let formattedSteps;
            formattedSteps = getBugReport.data.steps
            formattedSteps = formattedSteps.replace(/[0-9]\./g, '').split("\n").filter(err=>err);
            setStep(formattedSteps)
            // console.log("formattedsteps",formattedSteps);
        }
    },[])
    
    // useEffect(()=>{
    //     setLoading(true)
    //     fetch(`https://picsum.photos/v2/list?page=2&limit=10`)
    //     .then(response => response.json())
    //     // .then(value => console.log("value",value))
    //     .then(value => ( setImage(value.map(data=>data.download_url)),setLoading(false)))
    //     .catch(err=>{
    //         console.log(err);
    //     })
    // },[])

    // console.log("image",image);

    const datas = [
        {
        fileType:"image",
        file:"https://testingplatformbucket.s3.ap-south-1.amazonaws.com/2013_bmw_concept_ninety-1366x768.jpg"
        // file:"https://picsum.photos/id/1010/5184/3456"
    },
    {
        fileType:"image",
        file:"https://picsum.photos/id/1008/5616/3744"
    },
    {
        fileType:"image",
        file:"https://picsum.photos/id/101/2621/1747"
    },
    {
        fileType:"video",
        file:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
    },
    {
        fileType:"video",
        file:"https://testingplatformbucket.s3.ap-south-1.amazonaws.com/ForBiggerJoyrides.mp4"
        // file:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4"
    },
]

    const data = ["http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
    ]

    // console.log("data",data);

    const showAttachment = (e)=>{
        console.log("imageClick",e);
        setViewAttachmentModel(true)
        setAttachmentData(e)
    }

    const closeModel = (value)=>{
        console.log("closeModel",value);
        setViewAttachmentModel(value)
    }

    const video = ()=>{
        console.log("video");
        setViewAttachmentModel(true)
        // history("/videoPlayer")
    }

    // console.log("singleBugReportDataFromAPI",singleBugReportDataFromAPI.attachments);

    let bugPriority = singleBugReportDataFromAPI.bug_priority

    let bugStatus = singleBugReportDataFromAPI.bug_status 

    // console.log("bugStatus",bugStatus);

    let bugstatusColor = bugStatus === "CLOSED" ? "green" : "red"

    let steps = ["Go to login page", "Go to logout page", "click the button", "double tap the button" ]

    
    return(
        <ChakraProvider>
                {/* <Center>
                <Heading mb={"40px"} > BG 01 </Heading>
                </Center> */}

                {loading? <Center> <Spinner size='xl' textAlign={"center"} m={"120px"} /> </Center> :

                <>
                
                <Box  w="100%" pr={2} mt={"20px"} mb={"20px"} >
           <Flex  >
              <Spacer/>
              <Button bg={"red"} color="white" mr={"10px"} onClick={deleteBugReport}  >Delete </Button>
              <Button bg={"#6c757d"} color="white" mr={"10px"} onClick={editBugReport} >Edit Bug Report </Button>
          </Flex>
        </Box>

        {/* <Box fontWeight="bold" >
   <Flex>
    <Box w='100px' h='10'  > {singleBugReportDataFromAPI.tc_id} </Box>
    <Spacer />
    <Box w='100px' h='10' > {singleBugReportDataFromAPI.bug_id} </Box>
    <Spacer />
    <Box w='100px' h='10' > {singleBugReportDataFromAPI.last_tested} </Box>
   </Flex>
   </Box> */}

<SimpleGrid columns={3} spacing={10} fontSize={"20px"} fontWeight="bold" >
  <Box  textAlign="center" height='80px'> {singleBugReportDataFromAPI.tc_id} </Box>
  <Box  textAlign="center" height='80px' color={bugstatusColor}> {singleBugReportDataFromAPI.bug_id} </Box>
  <Box  textAlign="center" height='80px'> {singleBugReportDataFromAPI.last_tested}</Box>
</SimpleGrid>

        {/* { loading ? <Center> <Spinner size='xl' textAlign={"center"} m={"120px"} /> </Center>  :

    <Grid templateColumns='repeat(3, 1fr)' gap={10} fontSize={"20px"} fontWeight="bold" mr={"10px"} mb={"20px"}  lineHeight="20px" fontFamily="Inter" mt={"20px"} >
     <GridItem w='100%' h='10' > <Text ml={"15px"}  > {singleBugReportDataFromAPI.tc_id} </Text>  </GridItem>
     <GridItem w='100%' h='10' >  <Text  textAlign={"center"} color={bugstatusColor} >
          BR 01 
          {singleBugReportDataFromAPI.bug_id}
          </Text> </GridItem>
     <GridItem w='100%' h='10'> <Text textAlign="right" mr={"40px"} fontSize={"17px"} > 
     09-02-2021 
     {singleBugReportDataFromAPI.last_tested}
     </Text>  </GridItem>
    </Grid>  
       } */}

         {/* <Spacer>
        <Button justifyContent={"right"}  > Edit </Button>
         </Spacer> */}

                {/* <SimpleGrid column={3} columnGap={3} rowGap={6} w="full" >
                   <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > MODULE: </Text>
                   <Text alignItems={"center"} textAlign="center" justifyContent={"center"} fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > MODULE: </Text>
                </SimpleGrid> */}

            {/* <SimpleGrid columns={3} columnGap={3} rowGap={6} w="full" ml={"40px"} >
                <GridItem colSpan={1} >
                  <HStack>
                  <Text fontWeight="bold" pr={"20px"} > MODULE: </Text>
                    <Text  > Login screen </Text>
                    </HStack>
                </GridItem>
                <GridItem colSpan={1} >
                    <HStack>
                  <Text fontWeight="bold" pr={"20px"} > BUG STATUS </Text>
                    <Text> CLOSED </Text>
                    </HStack>
                </GridItem>
                <GridItem colSpan={1} >
                    <HStack>
                  <Text fontWeight="bold" pr={"20px"} > BUG STATUS </Text>
                    <Text> CLOSED </Text>
                    </HStack>
                </GridItem>
            </SimpleGrid> */}

        <Wrap justify='center' color="black" mb={"20px"} >   
        <WrapItem  >
        <Center w='350px' h='20px'   mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > MODULE: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > {singleBugReportDataFromAPI.module} </Text>
    </Center>
    </WrapItem>

    <WrapItem >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > BUG STATUS: </Text>
        {/* <Icon as ={FcApproval} w={8} h={10} ></Icon> */}
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > 
        {/* {bugStatus}  */}
        {singleBugReportDataFromAPI.bug_status}
        </Text>
             {/* <TiTick fontSize={"35px"} pb={"5px"} ></TiTick> */}
    </Center>
    </WrapItem>

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > BUG SEVERITY: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" >
             {/* HIGH */}
             {singleBugReportDataFromAPI.bug_severity}
              </Text>
    </Center>
    </WrapItem>

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > BUG PRIORITY: </Text>
        <Tooltip label={singleBugReportDataFromAPI.bug_priority} placement='top' >
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > 
        {bugPriority === "HIGH" ? <Icon as={AiTwotoneAlert} w={8} h={10} color={"red"} /> : bugPriority === "MEDIUM" ? 
        <Icon as={GoAlert} w={8} h={10} color={"red"} /> : <Icon as={IoAlertOutline} w={8} h={10} color={"red"} />  } 
        </Text>
        </Tooltip>
    </Center>
    </WrapItem>

    </Wrap>

    <Wrap justify='center' color="black" mb={"20px"} >

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > ISSUE TYPE: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > 
        {/* BACKEND  */}
        {singleBugReportDataFromAPI.issue_type}
        </Text>
    </Center>
    </WrapItem>

    <WrapItem  >
        <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > SERVER: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" >
             {/* STAGING  */}
             {singleBugReportDataFromAPI.server}
             </Text>
    </Center>
    </WrapItem>
    
    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > REPORTED BY: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > {singleBugReportDataFromAPI.reported_by} </Text>
    </Center>
    </WrapItem>

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > ASSIGNED TO: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > {singleBugReportDataFromAPI.assigned_to} </Text>
    </Center>
    </WrapItem>

    </Wrap>

    <Wrap justify='center' color="black" mb={"20px"} >

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > DEVICE: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > 
        {/* IPHONE */}
        {singleBugReportDataFromAPI.device}
         </Text>
    </Center>
    </WrapItem>
    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > DEVICE VERSION: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" >
             {/* 11 */}
             {singleBugReportDataFromAPI.device_version}
              </Text>
    </Center>
    </WrapItem>

    <WrapItem  >
        <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > BATTERY %: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" >
             {/* 90  */}
             {singleBugReportDataFromAPI.battery}
             </Text>
    </Center>
    </WrapItem>

    <WrapItem  >
    <Center w='350px' h='20px' fontSize='20px'  mb={"10px"} borderRadius={"20px"} >
        <Text fontSize={"18px"} fontWeight="bold" mr={"10px"} lineHeight="10px" fontFamily="Inter" > INTERNET SPEED: </Text>
        <Text fontSize={"17px"} lineHeight="10px" fontFamily="Inter" > 
        {/* 30 mbps */}
        {singleBugReportDataFromAPI.internet_speed}
         </Text>
    </Center>
    </WrapItem>

    </Wrap>

    <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       <Flex>
       <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter"> <span className=" bugField"> Bug Summary: </span> 
        {/* Create Project Create Project Create Project Create Project  */}
        {singleBugReportDataFromAPI.bug_summary}
        </Text>
       </Flex>
    </Wrap>
    
    <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       <Flex>
       <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter" > <span className=" bugField " > Expected Result: </span> 
       {/* Create Project Create Project Create Project Create Project  */}
       {singleBugReportDataFromAPI.expected_result}
       </Text>
       </Flex>
    </Wrap>

    <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       <Flex>
       <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter"> <span className=" bugField"> Actual Result: </span>
         {/* Create Project Create Project Create Project Create Project */}
         {singleBugReportDataFromAPI.actual_result}
          </Text>
       </Flex>
    </Wrap>

    

    {/* <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       <Flex>
       <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter"> <span className=" bugField"> Steps To Reproduce: </span>  <OrderedList>
  <ListItem>Lorem ipsum dolor sit amet</ListItem>
  <ListItem>Consectetur adipiscing elit</ListItem>
  <ListItem>Integer molestie lorem at massa</ListItem>
  <ListItem>Facilisis in pretium nisl aliquet</ListItem>
</OrderedList> </Text>
       </Flex>
    </Wrap> */}

    <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       {/* <Flex> */}
       <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter"> <span className=" bugField"> Steps To Reproduce: </span> 
        {/* <OrderedList>
            {step.map(data=>{
                return<ListItem> {data} </ListItem>
            })}
        </OrderedList> */}
       </Text>
       {/* </Flex> */}
    </Wrap>

    <Wrap justify='left' ml="55px" color="black"  > 
    {/* <Text fontSize={"17px"} lineHeight="30px" fontFamily="Inter"> <span className=" bugField"> Steps To Reproduce: </span>  */}
        <OrderedList>
            {step.map((data,index)=>{
                return<ListItem key={index} > {data} </ListItem>
            })}
        </OrderedList>
        {/* </Text> */}
    </Wrap>

    <Wrap justify='left' ml="40px" color="black" mt={"20px"} >   
       <Flex>
       <Text  lineHeight="30px" fontFamily="Inter" fontWeight="bold" fontSize="18px" > Attachments: </Text>
       </Flex>
    </Wrap>

    <Wrap justify='left' ml="35px" color="black" mt={"20px"} mb={"50px"} >   
    {/* <Box boxSize='sm'> */}
    {/* <Image src='https://bit.ly/dan-abramov' cursor={"pointer"} boxSize='150px' objectFit='cover' alt='Dan Abramov' onClick={imageClick} />
    <Image src='https://bit.ly/dan-abramov' cursor={"pointer"} boxSize='150px' objectFit='cover' alt='Dan Abramov' onClick={imageClick} />
    <Image src='https://source.unsplash.com/user/c_v_r/' cursor={"pointer"} boxSize='150px' objectFit='cover' alt='Dan Abramov' onClick={video} /> */}
    {/* </Box> */}

   {/* <video preload="metadata" cursor={"pointer"}   onClick={video} style={{width:"300px", height:"150px"}} >
        <source src="http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4#t=5"  type="video/mp4" />
   </video> */}

   {/* {
       image.map(data=>{
           return <Image src={data} cursor={"pointer"} boxSize='150px' objectFit='cover' alt='Dan Abramov' onClick={imageClick.bind(null,data) } />
       })
   } */}

   {/* {
      data.map(data=>{
           return  <video preload="metadata" className="videoTag"   onClick={imageClick.bind(null,data)} style={{width:"300px", height:"150px"}} >
           <source src={data}  type="video/mp4" />
      </video>
       })
   } */}

   { singleBugReportDataFromAPI?.attachments?.length >0 ? singleBugReportDataFromAPI.attachments.map(data=>{
       if(data.fileType.includes("image")){
           return <Image src={data.file} cursor={"pointer"} key={data._id} boxSize='200px' objectFit='cover' alt='Dan Abramov' onClick={showAttachment.bind(null,data) } />
       }else{
           return <video preload="metadata" className="videoTag" key={data._id}  onClick={showAttachment.bind(null,data)} style={{width:"200px", height:"200px"}} >
           <source src={data.file}  type="video/mp4" />
      </video>
       }
   }) : <Text>  No Attachments Found.  </Text> }

    </Wrap>


                </>
                


                }

         

    { viewAttachmentModel && <ImageModel showModel={true} setShow={closeModel} image={attachmentData} />}




    </ChakraProvider>
    )
}

export default ViewBugReport