import { useState } from "react"
import {Modal, Button} from "react-bootstrap"


const ImageModel = (props)=>{

    console.log("props",props);

    const handleclose = ()=>{
        console.log("handleclose");
        props.setShow(false)
    }

    return(
        <Modal
        show={props.showModel}
        onHide={handleclose}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body  >
        { props.image.fileType.includes("image") ? <img src={props.image.file} className="img-fluid center-block d-block mx-auto" style={{maxHeight:"500px"}} /> :
        <video metadata controls className="embed-responsive-item" style={{maxHeight:"500px",width:"100%"}} >
        <source src={props.image.file} type="video/mp4" />
      </video>
        }

      {/* <video metadata controls className="embed-responsive-item" style={{width:"100%"}} >
        <source src={props.image} type="video/mp4" />
      </video> */}
      {/* <iframe class="embed-responsive-item" src="http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4" width={"300px"}  className="center-block d-block mx-auto" ></iframe> */}
        {/* <img src={props.image}  className="img-fluid center-block d-block mx-auto" ></img> */}
      </Modal.Body>
     
    </Modal>
    )
}

export default ImageModel