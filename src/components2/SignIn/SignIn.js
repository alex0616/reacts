import { useState } from "react"
import GoogleLogin from "react-google-login"
import { useNavigate } from "react-router-dom"

import "./SignIn.css"

import axios from "axios"

import { useToast } from '@chakra-ui/react'


const SignIn = ({loginData, setLoginData})=>{

  const toast = useToast()


  const toast1 = (title,data,status)=>{
    console.log("data,status",data,status);
   return toast({
        title: title,
        description: data,
        status: status,
        position:"bottom-right",
        duration: 5000,
        isClosable: true,
      })
}

    // const [loginData, setLoginData] = useState(
    //     localStorage.getItem('loginData') ? JSON.parse(localStorage.getItem("loginData")) : null )

        // const history = useNavigate()

    const onSuccessHandler = async (googleData)=>{
        console.log("googleData",googleData,googleData.profileObj);
        console.log("tokenId",googleData.tokenId);

        const signIn = await axios.post("http://localhost:8081/user/login",{},{
          headers:{
            "id_token":googleData.tokenId
          }
        }).then(data=>data)
        .catch(err=>{
          console.log("err",err.response);
          let error = err.response ? err.response.data.error : "Something went wrong" 
            console.log(error);
            toast1("Error", error,"error")
        })

        if(signIn.status===200){
          console.log(signIn);
          localStorage.setItem("loginData",JSON.stringify(signIn.data.token))
          setLoginData(signIn.data.token)
          toast1( "SignedIn Successfully !", `Welcome, ${signIn.data.user.name} ` ,"success")
        }

        // localStorage.setItem("loginData",JSON.stringify(googleData.profileObj))
        // setLoginData(googleData.profileObj)
        // props.storedData(true)
        // history('/project')
      }
    
      const onFailureHandler = (result)=>{
        console.log("result",result);
      }

      // console.log(props);

    return(
      // <div className="text-center mx-auto googleLoginDiv " >
      <div className="mains text-center" >
      <GoogleLogin className="button" 
      clientId={"722156133723-hdggnjeaa2kocs21psd26o61f4p10qbu.apps.googleusercontent.com"}
      buttonText="Please sign-in"
      onSuccess={onSuccessHandler}
      onFailure={onFailureHandler}
      cookiePolicy={"single_host_origin"}
      >
      </GoogleLogin>
      </div>
    )
}

export default SignIn