import { Box, Button, ChakraProvider, Flex, Spacer,Wrap, WrapItem, Center, Spinner,Text, Tooltip as Tool ,Container, Heading, Icon, Input, HStack ,SimpleGrid,  Stat, useColorModeValue,StatLabel,StatNumber, VStack,    } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TestCase from "../TestCase/TestCase";
import "./ProjectLandingPage.css"

import SearchModel from "./SearchModel"

import axios from "axios"

import {MdOutlineDevices} from "react-icons/md"

import {HiTemplate} from "react-icons/hi"

import {VscSymbolArray} from "react-icons/vsc"

import {ImDatabase} from "react-icons/im"

import {EditIcon, DeleteIcon} from "@chakra-ui/icons"

import { useToast } from '@chakra-ui/react'

import { BsPerson } from 'react-icons/bs';
import { FiServer } from 'react-icons/fi';
import { GoLocation } from 'react-icons/go';

import {Bar, Line, Pie, Doughnut} from 'react-chartjs-2';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
  } from 'chart.js';
import AddProjectModel from "../projectList/AddProjectModel";

  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
  );


const ProjectLandingPage = () => {

    // const [testCase, setTestCase] = useState(false)
    
    const [fetchData, setFetchData] = useState("")

    const [ userName, setUserName ] = useState([])

    const [dashBoardData,setDashBoardData] = useState("")

    const [bugStatus, setBugStatus] = useState("")

    const [bugSeverity, setBugSeverity] = useState("")

    const [issueType, setIssueType ] = useState("")

    const [testCaseData, setTestCaseData] = useState("")

    const [bugData,setBugData] = useState("")

    const [showSearchModel, setShowSearchModel] = useState(false)

    const [showEditProjectModel, setShowEditProjectModel] = useState(false)

    const [reload, setReload] = useState(false)

    const [loading, setLoading] = useState()
 

    // const [loading, setLoading] = useState(false)

    const [projectDetails, setProjectDetails] = useState("")

    

    let userAge = [
        20,
        30,
        40,
        50,
        60,
        70,
        80,
        90,
        100,
        110
    ]

    const history = useNavigate()

    const {projectId} = useParams()

    // console.log("params",params);

    //useLayOuteffect
    
    const toast = useToast()
    
    const toast1 = (data,status)=>{
      console.log("data,status",data,status);
     return toast({
          title: status,
          description: data,
          status: status,
          position:"bottom-right",
          duration: 5000,
          isClosable: true,
        })
  }

    useEffect(()=>{

        // const sampleData = fetch(`https://jsonplaceholder.typicode.com/todos/${projectId}`)
        // .then(response => response.json())
        // .then(json => (console.log(json),setFetchData(json)))
        // .catch(err=>{
        //     console.log(err);
        // })

        // fetch("https://jsonplaceholder.typicode.com/users")
        // .then(response => response.json())
        // .then(value => setUserName(prevData=> value.map(data=>data.name ) )  )
        // .catch(err=>{
        //     console.log(err);
        // })
        // return(()=>{
        //   alert("hello")
        // })

    },[])

    // console.log("users",userName);

    // const testCaseHandler = ()=>{
    //     console.log("testCaseHandler");
    //     setTestCase(true)
    // }

    const testCasePageHandler = ()=>{
        console.log("testCasePageHandler");
        history(`/testCase/${projectId}`)
        // history(`/testCase/${fetchData.id}`)
    }

    const bugReportPageHandler = ()=>{
        console.log("bugReportPageHandler");
        history(`/bugReport/${projectId}`)    
    }

    useEffect(()=>{

      setLoading(true)

      axios.get(`http://localhost:8081/dashboard/bug-count/${projectId}`)
      .then(data=>(console.log("data.data",data.data), setBugData(data.data),
      setLoading(false)
      ))
      .catch(err=>{
        console.log("err",err.response);
      })

      // axios.get(`http://localhost:8081/dashboard/testcases-with-status/${projectId}`)
      // .then(data=>(console.log("data",data),
      // setTestCase(data.data),
      // setLoading(false)
      // ))
      // .catch(err=>{
      //   console.log("err",err.response);
      // })


      // return(()=>{
      //   setReload(false)
      //   // alert("hello")
      // })

    },[])

    console.log("bugdatabugdata",bugData.issueTypeCount);

    useEffect(()=>{

      setLoading(true)

       axios.get(`http://localhost:8081/dashboard/testcases-with-status/${projectId}`)
      .then(data=>(console.log("data",data),
      setTestCaseData(data.data),
      setLoading(false)
      ))
      .catch(err=>{
        console.log("err",err.response);
      })

      return(()=>{
        setReload(false)
      })

    },[reload])

    // console.log("dashBoardData",dashBoardData);

    const bugStatusData = {
      labels:['OPEN', 'REOPNED','CLOSED' ],
      datasets:[
          {
              label:"population",
              data:[
                bugData.bugStatusCount?.openCount,
                bugData.bugStatusCount?.reopenedCount,
                bugData.bugStatusCount?.closedCount,
                // dashBoardData.bugStatusCount && dashBoardData.bugStatusCount.openCount,
                // dashBoardData.bugStatusCount && dashBoardData.bugStatusCount.closedCount,
                // dashBoardData.bugStatusCount && dashBoardData.bugStatusCount.reopenedCount,
              ],
              backgroundColor:[
                'red',
                "#00A5FF",
                '#20C9AC',
                ]
          }
      ],
  }

  const bugSeverityData = {
    labels:['LOW', 'MEDIUM','CRITICAL' ],
    datasets:[
        {
            label:"population",
            data:[
              bugData.bugSeverityCount?.lowCount,
              bugData.bugSeverityCount?.mediumCount,
              bugData.bugSeverityCount?.criticalCount,
            ],
            backgroundColor:[
              "#f7e6e5",
              '#eeb8b4',
              '#fc9393',
              ]
        }
    ],
}

 const testCaseChartData = {
  labels:['PASS', 'FAIL' ],
  datasets:[
      {
          label:"population",
          data:[
            testCaseData.passCount,
            testCaseData.failCount
          ],
          backgroundColor:[
            '#20C9AC',
            "#00A5FF"
            ]
      }
  ],
}

    const data = {
        // labels:userName,
        labels:['PASS', 'FAIL' ],
        datasets:[
            {
                label:"population",
                // data:userAge,
                data:[
                    10,
                    15,
                ],
                backgroundColor:[
                    '#20C9AC',
                     "#00A5FF"
                    // 'red',
                  ]
            }
        ],
    }

    const showSearch = ()=>{
      console.log("showSearch");
      setShowSearchModel(true)
    }

    const closeModel = ()=>{
      console.log("close");
      setShowSearchModel(false)
    }

    const deleteProject = async()=>{
      console.log("deleteProject");
      const deleteProject = await axios.delete(`http://localhost:8081/project/delete-project/${projectId}`)
      .then(data=>data)
      .catch(err=>{
        console.log("err",err.response);
        let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
      })
      if(deleteProject.status===200){
        toast1("Project deleted Successfully","success")
        history('/project')
      }
    }

    const editProject = async()=>{
      console.log("editProject");
      const getProjectDetails = await axios.get(`http://localhost:8081/project/get-project-by-id/${projectId}`)
      .then(data=>( setProjectDetails(data.data),console.log("data",data)))
      .catch(err=>{
        console.log("err",err.response);
      })
      setShowEditProjectModel(true)
    }

    const submitProject = ()=>{
      console.log("submitProject");
      setReload(true)
  }

  const closeForm = ()=>{
      console.log("closeForm");
      setShowEditProjectModel(false)
  }

  return (
    <ChakraProvider className="chakraprovider" >
      {/* <Box  w="100%" p={4} >
          <Flex >
            <Text fontWeight={"bold"} fontSize={"20px"}>Project Name </Text>
              <Spacer/>
              <Button bg={"white"} placeholder='Search' width="50" mr={"20px"} onClick={showSearch} > search </Button>
              <Button bg={"#6c757d"} color="white" mr={"10px"} onClick={testCasePageHandler} > Test Cases </Button>
              <Button bg={"#6c757d"} color="white" mr={"10px"} onClick= {bugReportPageHandler} > Bug Report </Button>
          </Flex>
      </Box> */}

      {loading ? <Center> <Spinner size='xl' m={"120px"} /> </Center> :
      
      <>
      <Box mt="5" >
<Flex>
  <Box p='2'>
    <Heading size='md'>{testCaseData.name}</Heading>
  </Box>
  <Spacer />
  <Box>
  <Button bg={"white"} mr='4' mb='4' onClick={showSearch} >
      Search
    </Button>
    <Button bg={"#6c757d"} color="white" onClick={testCasePageHandler} mr='4' mb='4'>
    Test Cases
    </Button>
    <Button bg={"#6c757d"} color="white" onClick= {bugReportPageHandler} mr='4' mb='4'>Bug Report</Button>
  </Box>
</Flex>
</Box>

<Box  w="100%" p={4} color={"white"}  >
          <Flex >
              <Spacer/>
              <Tool label='Delete Project' >
          <Button  bg='red.500' mr={"20px"} onClick={deleteProject  } >
            <DeleteIcon  w={6} h={6}  />
          </Button>
          </Tool>

          <Tool label="Edit Project" >
          <Button bg={"#0d6efd"} onClick={editProject} >
            <EditIcon w={6} h={6} />
          </Button>
          </Tool>
          </Flex>
      </Box>

      { showEditProjectModel && <AddProjectModel showForm={true} projectDetails={projectDetails} submitProject={submitProject} closeShowAddProjectFormModel={closeForm} />}

      {showSearchModel && <SearchModel show={showSearchModel} close={closeModel} />}

      <Center>
       <VStack>
       <Heading borderBottom={"2px solid #4056A1"} fontSize="28" > Issue Type </Heading> 
       <Flex>
    <Text className="chartHeading"> Total Count : </Text>
    <Text className="TotalCount" fontSize={"large"} fontWeight="bold" mt={"10px"} > {bugData.issueTypeCount?.totalCount} </Text>
    </Flex>
    </VStack>

       </Center>
    <Wrap ml={"10px"} mr={"10px"} justify='center' mt={"20px"} color="White" >
      
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"} >
      <VStack>
      <Icon as={ImDatabase} w={8} h={8}  />
      <Flex>
    < Text fontSize={"20px"} pr={"10px"} > Backend:  </Text>
    < Text fontSize={"20px"}  > {bugData.issueTypeCount?.backendCount}  </Text>
    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"} >
    <VStack>
    <Icon as={MdOutlineDevices} w={8} h={8} />
    <Flex>
    < Text fontSize={"20px"} pr={"10px"} > Device specific: </Text>
    < Text fontSize={"20px"}  > {bugData.issueTypeCount?.deviceSpecificCount}  </Text>

    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"}  >
      <VStack>
    <Icon as={HiTemplate} w={8} h={8}  />
    <Flex>
    < Text fontSize={"20px"} pr={"10px"} > User Interface: </Text>
    < Text fontSize={"20px"}  > {bugData.issueTypeCount?.uiCount}  </Text>
    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    </Wrap>
    <Center>
    </Center>

      <div className="" >

          <div className="row main mb-3" >
      
              <div className="col-lg-4 col-md-6 mb-3" >
                <div className="chart1 " >
                  <Center>
                    <Text className="chartHeading" > TEST CASES </Text>
                  </Center>
                  {testCaseData.passCount + testCaseData.failCount !=0 ?
                  <>
                  <div className="pieDiv" > 
                  
                  {/* <Pie data={data}  height="300px" width="300px" options={{ maintainAspectRatio: false }} ></Pie> */}
                  <Pie data={testCaseChartData}  height="350px" width="350px" options={{ maintainAspectRatio: false }} ></Pie>
                  
                  {/* <Pie data={data} width={"500px"} height={"300px"} ></Pie> */}
                  </div>
                  <Center>
                    <Text className="chartHeading">
                      Total Count : {testCaseData.totalCount}
                    </Text>
                  </Center>
                  </>
                  :
                  <Text className="d-flex justify-content-center align-items-center h-75 text-secondary "  > No Data Found </Text>
                }
                  
                  </div>
              </div>

              

              <div className="col-lg-4 col-md-6 mb-3"  >
              <div className="chart1 " >
              <Center>
                    <Text className="chartHeading" > BUG STATUS  </Text>
                  </Center>

                  { bugData.bugStatusCount?.openCount + bugData.bugStatusCount?.reopenedCount + bugData.bugStatusCount?.closedCount != 0 ? 
                  <>
                  <div className="pieDiv" >
                  <Doughnut data={bugStatusData} height="350px" width="350px" options={{ maintainAspectRatio: false, cutout: 100 }} ></Doughnut>
                  </div>
                  <Center>
                    <Text className="chartHeading">
                      Total Count : {bugData.bugStatusCount?.totalCount}
                    </Text>
                  </Center> 
                  </>
                    :
                    <Text className="d-flex justify-content-center align-items-center h-75 text-secondary "  > No Data Found </Text>
                }
                  
                  </div>   
              </div>              


              <div className="col-lg-4 col-md-12 mb-3"  >
              <div className="chart1 " >
              <Center>
                    <Text className="chartHeading" > BUG SEVERITY </Text>
                  </Center>
                  { bugData.bugSeverityCount?.lowCount + bugData.bugSeverityCount?.mediumCount + bugData.bugSeverityCount?.criticalCount != 0 ? 
                  <>
                  <div className="pieDiv" >
                  <Doughnut data={bugSeverityData} height="350px" width="350px" options={{ maintainAspectRatio: false, cutout: 100 }} ></Doughnut>
                  </div>
                  <Center>
                    <Text className="chartHeading" >
                      Total Count : {bugData.bugSeverityCount?.totalCount}
                    </Text>
                  </Center>
                  </>
                  :
                  <Text className="d-flex justify-content-center align-items-center h-75 text-secondary "  > No Data Found </Text>
                }
                       
                  </div>          
              </div>             
          </div>        
      </div>
      
      </>
      
      }



      {/* <div className="center" >
        <p> Hello world </p>
      </div> */}

      {/* <div className="container" >
        <div className="row" >
          <Center>
          <div className="col issueType" >
            <Center>
            <Text >
              Issue Type
            </Text>
            </Center>

            <div className="issueTypeList" >
            <Text >
              BackEnd: 01
            </Text>
            </div>
            
          </div>
          </Center>
        </div>

      </div> */}

      {/* <Center>

      <Container border={"2px solid red"} mb={"20px"} height={"300px"} ml={"15px"} mr={"15px"} >
        <Center>
        <Heading borderBottom={"2px solid red"} fontSize={"30"} > Issue Type </Heading>
        </Center>

        <Center>
        <HStack mt={"20px"}  >

        <HStack mt={"20px"} ml={"50px"} >
          <Icon as={VscSymbolArray} w={12} h={12} color='red.500' />
          <Text fontSize={25} > BACKEND: </Text>
          <Text pl={"40px"} > 01 </Text>
        </HStack>
        </Center>

        <Center>
        <HStack mt={"20px"}  >

        <HStack mt={"20px"} ml={"50px"} >
          <Icon as={MdOutlineDevices} w={12} h={12} color='red.500' />
          <Text fontSize={25} > DEVICE SPECFIC: </Text>
          <Text> 01 </Text>
        </HStack>
        </Center>

        <Center>
        <HStack mt={"20px"} ml={"50px"} >
        <HStack mt={"20px"}  >

          <Icon as={HiTemplate} w={12} h={12} color='red.500' />
          <Text> USER INTERFACE: </Text>
          <Text > 01 </Text>
        </HStack>
        </Center>


        <Center>
        <HStack mt={"20px"}  >
          <Text> Total Count :  </Text>
          <Text> 10 </Text>
          </HStack>
        </Center>
        
      </Container>
      </Center> */}

     {/* <Center bg="aqua"> */}

     
     {/* <Center>
       <VStack>
       <Heading borderBottom={"2px solid gray"} fontSize="28" > Issue Type </Heading> 
       <Flex>
    <Text className="chartHeading"> Total Count : </Text>
    <Text className="TotalCount" fontSize={"large"} fontWeight="bold" mt={"10px"} > 10 </Text>
    </Flex>
    </VStack>

       </Center>
    <Wrap ml={"10px"} mr={"10px"} justify='center' mt={"20px"} color="White" >
      
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"} >
      <VStack>
      <Icon as={ImDatabase} w={8} h={8}  />
      <Flex>
    < Text fontSize={"20px"} pr={"10px"} > Backend:  </Text>
    < Text fontSize={"20px"}  > 01  </Text>
    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"} >
    <VStack>
    <Icon as={MdOutlineDevices} w={8} h={8} />
    <Flex>
    < Text fontSize={"20px"} pr={"10px"} > Device specfic: </Text>
    < Text fontSize={"20px"}  > 01  </Text>

    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    <WrapItem   >
    <Center w='250px' h='90px' fontSize='20px' bg={"#4056A1"} border={"2px solid black"} mb={"10px"} borderRadius={"20px"}  >
      <VStack>
    <Icon as={HiTemplate} w={8} h={8}  />
    <Flex>
    < Text fontSize={"20px"} pr={"10px"} > User Interface: </Text>
    < Text fontSize={"20px"}  > 01  </Text>
    </Flex>
    </VStack>
    </Center>
    </WrapItem>
    </Wrap>
    <Center>
    </Center> */}





    {/* </Center> */}
      

      {/* <Wrap spacing='30px' justify='center' border="2px solid red" >
  <WrapItem>
    <Center w='180px' h='80px' border={"2px solid green"} >
      Box 1
    </Center>
  </WrapItem>
  <WrapItem>
    <Center w='180px' h='80px' bg='green.200'>
      Box 2
    </Center>
  </WrapItem>
  <WrapItem>
    <Center w='180px' h='80px' bg='tomato'>
      Box 3
    </Center>
  </WrapItem>
</Wrap> */}

      {/* <SimpleGrid columns={{ base: 1, md: 3 }} spacing={{ base: 5, lg: 8 }}>
      <Stat
      px={{ base: 2, md: 4 }}
      py={'5'}
      shadow={'xl'}
      border={'1px solid'}
      borderColor={useColorModeValue('gray.800', 'gray.500')}
      rounded={'lg'}>
      <Flex justifyContent={'space-between'}>
        <Box pl={{ base: 2, md: 4 }}>
          <StatLabel fontWeight={'medium'} isTruncated>
            {"title"}
          </StatLabel>
          <StatNumber fontSize={'2xl'} fontWeight={'medium'}>
            {"stat"}
          </StatNumber>
        </Box>
        <Box
          my={'auto'}
          color={useColorModeValue('gray.800', 'gray.200')}
          alignContent={'center'}>
          
        </Box>
      </Flex>
    </Stat>
      </SimpleGrid> */}

      




     

     

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      {/* <p> {fetchData.title} </p>
      <p> {fetchData.id} </p>
      <p> {fetchData.userId} </p> */}





      


  


      {/* <Bar data = {data}  options={{
            title:{
              display:true,
              text:'Largest Cities In ',
              fontSize:25
            },
            legend:{
              display:true,
              position:"right"
            }
          }} >
        </Bar>

        <Pie data={data} width={"500px"} height={"300px"} ></Pie> */}


        {/* <div className="chart-container">
    <canvas id="myCanvas"></canvas>
    <Doughnut data={data} ></Doughnut>

    </div> */}

      {/* { testCase && <TestCase/>} */}
    </ChakraProvider>
  );
};

export default ProjectLandingPage;
