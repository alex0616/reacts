import { ChakraProvider, Input,Flex, Heading, Wrap, WrapItem,Center, Text, Box,InputGroup,InputLeftElement } from "@chakra-ui/react"
import {Modal, Button} from "react-bootstrap"

import {SearchIcon} from '@chakra-ui/icons' 

import { useNavigate, useParams } from "react-router-dom"
import { useEffect, useRef, useState } from "react"

import "./SearchModel.css"

import axios from "axios"


const SearchModel = (props)=>{

    const closeModel = ()=>{
        props.close()
    }

    const {projectId} = useParams()

    const history = useNavigate()

    const [searchResult, setSearchResult] = useState([])

    const inputRef = useRef()

    console.log("projectId",projectId);


    const search = async(e)=>{
        console.log("search",e.target.value);

        // if(!e.target.value){
        //     console.log("e.target.value");
        //     setSearchResult([])
        //     return
        // }

        const body = {
            project_id:projectId,
            keyword:e.target.value
        }
        const search = await axios.post("http://localhost:8081/project/search",body)
        .then(data=>( setSearchResult(data.data), console.log("data",data)))
        .catch(err=>{
            setSearchResult([])
            console.log("err",err.response);
        })
    }

    const page = (data)=>{
        console.log("page",data);

        if(data.tc_id && data.bug_id ){
            console.log("data.tc_id && data.bug_id");
            history(`/viewBugReport/${data.bug_id}`)
        }else{
            console.log("data.tc_id");
            history(`/testCase/${projectId}`,{state:{data:data}})
        }
    }

    return(
        <Modal show={props.show} onHide={closeModel} onShow={() => {
            inputRef.current.focus();
          }}
     >
        <Modal.Header closeButton>
          <Modal.Title> 

              <Flex>
              {/* <SearchIcon mt={"10px"} mr={"5px"} color={"#4056A1"} />  */}

              {/* <input placeholder="Search" className="searchInput" /> */}

              <InputGroup>
             <InputLeftElement
            pointerEvents='none'
            children={<SearchIcon color='#4056A1' />}
            />
    <Input border={"none"} variant='flushed' width={"400px"}  placeholder='Search' onChange={search} ref={inputRef} /> 
    </InputGroup>

              {/* <Input placeholder="Search" border={"none"} width={100}  onChange={search}/> */}

              </Flex>

               </Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-dialog-scrollable modelBody " >
            
            {/* Woohoo, you're reading this text in a modal! */}
            
            {/* <Wrap ml={"10px"} mr={"10px"} justify='center'   >   
             <WrapItem cursor={"pointer"}  >
                 <Box pl={"10px"} w="100%" h='80px' bg='' color={"black"} border="2px solid black" mb={"10px"} borderRadius={"10px"}>
                 <Text mb={"20px"} > TC01 </Text>
                 < Text fontSize={"20px"} lineHeight="10px" fontFamily="Inter" > Create Project </Text>
            </Box>
            </WrapItem>
            
            </Wrap> */}

     {/* <Box p={3} shadow='md' borderWidth='1px' mb={4} >
      <Text >Plan Money</Text>
      <Heading fontSize='xl' mt={2}>Orders Module</Heading>
      </Box>

      <Box p={3} shadow='md' borderWidth='1px' mb={4} >
      <Text >Plan Money</Text>
      <Heading fontSize='xl' mt={2}>Orders Module</Heading>
      </Box> */}

      { searchResult.length ? searchResult.map(data=>{
          return<Box p={3} shadow='md' borderWidth='1px' mb={4} key={data._id} onClick={page.bind(null,data)} cursor={"pointer"}  >
          <Text >{ data.bug_id ? data.bug_id : data.tc_id  }</Text>
          <Heading fontSize='xl' mt={2}>{data.module}</Heading>
          </Box>
      }) : <Center> <Text> No Data Found. </Text> </Center> }

            </Modal.Body>

        {/* <Modal.Footer>
          <Button variant="secondary" >
            Close
          </Button>
          <Button variant="primary" >
            Save Changes
          </Button>
        </Modal.Footer> */}
      </Modal>
      
    )
}


export default SearchModel