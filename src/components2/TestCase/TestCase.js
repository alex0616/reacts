import { ChakraProvider, Box, Button, Flex, Spacer, HStack, Text, Spinner, GridItem,FormLabel,Input, SimpleGrid, Tooltip, Table, Thead, Tbody, Tr, Th, Td, chakra ,Center, border, background, color, Heading} from "@chakra-ui/react"
import { TriangleDownIcon, TriangleUpIcon } from '@chakra-ui/icons'
import { useLocation, useNavigate, useParams } from "react-router-dom";
import "./TestCase.css"
import { useTable, useSortBy } from 'react-table';
import DataTable, { createTheme,Alignment }  from "react-data-table-component";

import movies from "./Movies";
import paginationFactory from "react-bootstrap-table2-paginator";

import React, { useEffect, useState } from "react"
import Model from "./Model";
import StatusModel from "./StatusModel";

import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";


import axios from "axios"

const TestCase = ()=>{

  const [showStatusModel, setShowStatusModel] = useState(false)

  const [testCaseStatus, setTestCaseStatus] = useState()

  const [reload, setReload] = useState(false)

  const [startDate, setStartDate] = useState("")

  const [endDate, setEndDate] = useState("")
  
  const [validStartDate, setValidStartDate] = useState()

  const [validEndDate, setValidEndDate] = useState()

  const [startEndDateIsValid, setStartEndDateIsValid] = useState(false)

  const [loading, setLoading] = useState(true)

    const history = useNavigate()

    const location = useLocation()

    // console.log(location.state.data);
   
    
    const [ showModel , setShowModel ] = useState( location.state? true : false)

    const [ testCase, setTestCase ] = useState( location.state? location.state.data : {})

    const {projectId} = useParams()

    // const [reloadList, setReloadList] = useState(false)

    // console.log(projectId);

    const [testCaseDataFromAPI,  setTestCaseDataFromAPI] = useState([])

    // useEffect(()=>{
    //     const body = {
    //         startDate:startDate,
    //         endDate:endDate
    //     }
    //     const getTestCase = axios.post(`http://localhost:8081/testcase/get-all-testcases/${projectId}`, body )
    //     .then(data=>( setTestCaseDataFromAPI(data.data), console.log("data from API",data.data)))
    //     .catch(err=>{
    //         console.log("err",err.response);
    //     })
    //     return(()=>{
    //         setStartEndDateIsValid(false)
    //     })

    // },[startEndDateIsValid])

    useEffect(()=>{
        // setLoading(true)
        const body = {
            startDate:startDate,
            endDate:endDate
            // "startDate":"2022-02-14",
            // "endDate":"2022-02-15"
        }

        // if(startEndDateIsValid){
        //     console.log("startEndDateIsValid");
        //     const getTestCase = axios.post(`http://localhost:8081/testcase/get-all-testcases/${projectId}`, body )
        // .then(data=>( setTestCaseDataFromAPI(data.data), console.log("data from",data.data)))
        // .catch(err=>{
        //     console.log("err",err.response);
        // })
        // }else{
        //     console.log(body);
        //     const getTestCase = axios.post(`http://localhost:8081/testcase/get-all-testcases/${projectId}`,)
        //     .then(data=>( setTestCaseDataFromAPI(data.data), console.log("data",data.data)))
        //     .catch(err=>{
        //         console.log("err",err.response);
        //     })
        // }
        const getTestCase = axios.post(`http://localhost:8081/testcase/get-all-testcases/${projectId}`, startEndDateIsValid && body )
            .then(data=>( setTestCaseDataFromAPI(data.data), console.log("data",data.data),setLoading(false)))
            .catch(err=>{
                console.log("err",err.response);
            })
        return(()=>{
            setReload(false)
        })
    },[reload])

    const createNewTestCase = ()=>{
        history(`/addTestCase/${projectId}`)
    }

    const bugReport = ()=>{
        console.log("bugReport");
        history(`/bugReport/${projectId}`)
    }

    const conditionalRowStyles = [
        {
          when: row => row.status === "FAIL",
          style: {
            backgroundColor: '#f5d6d6',
          },
        },
      ];

      const testCaseStatuses = (e,id)=>{
          console.log("testCaseStatuses",e,id);
          setShowStatusModel(true)
          setTestCaseStatus({e,id})
      }

    //   console.log("testCaseStatusToModel",testCaseStatusToModel);

      const testCaseReload = ()=>{
        //   console.log("submitTestCaseStatus");
          setReload(true)
      }

      const column = [
        {
          id: 1,
          name: "TC ID",
          selector: (row) => row.tc_id,
          sortable: true,
          reorder: true,
          width:"130px"
        //   maxWidth:"40px"
        },
        {
            id: 2,
            name: "REPORTED ON",
            selector: (row) => row.reported_on,
            sortable: true,
            reorder: true,
            maxWidth:"350px"
          },
        {
          id: 3,
          name: "MODULE",
          selector: (row) => row.module,
          sortable: true,
          reorder: true,
          maxWidth:"350px"
        },
        {
          id: 4,
          name: "TEST SCENARIO",
          selector: (row) => row.test_scenario,
          sortable: true,
          reorder: true
        },
        {
            id: 5,
            name: "STATUS",
            selector: (row) =><div onClick={testCaseStatuses.bind(null,row.status,row._id)} > {row.status} </div>,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"140px"
          },
      ];

        const columns = [
            {
              id: 1,
              name: "TC ID",
              selector: (row) => row.id,
              sortable: true,
              reorder: true,
              maxWidth:"50px"
            },
            {
              id: 2,
              name: "MODULE",
              selector: (row) => row.title,
              sortable: true,
              reorder: true,
              maxWidth:"350px"
            },
            {
              id: 3,
              name: "TEST SCENARIO",
              selector: (row) => row.actors,
              sortable: true,
              reorder: true
            },
            {
                id: 4,
                name: "STATUS",
                selector: (row) =><div onClick={testCaseStatuses.bind(null,row.status,row._id)} > {row.status} </div>,
                sortable: true,
                // right: true,
                reorder: true,
                // center:true,
                maxWidth:"140px"
              },
          ];


        const customStyles = {
            table:{
                style:{
                    width:"95%",
                    margin:"auto",
                    border:"5px solid #6c757d",
                    backgroundColor:"#6c757d"
                }
            },
            head:{
                style:{
                    fontSize:"32",
                    fontWeight: "bold",
                }
            },
            rows: {
                style: {
                    // minHeight: '72px', // override the row height
                    fontSize:"32",
                },
            },
            headCells: {
                style: {
                    paddingLeft: '18px', // override the cell padding for head cells
                    // paddingRight: '8px',
                    // width:"50%",
                },                
            },
            cells: {
                style: {
                    color:"black",
                    fontFamily:"Inter",
                    // background:"green"
                    paddingLeft: '18px', // override the cell padding for data cells
                    // paddingRight: '8px',
                },
            },
        };

        const rowEvents = (e)=>{
            setTestCase(e)
            setShowModel(true)
        }

        const setShow = (value)=>{
            console.log("value",value);
            value === "testCaseModel" ? setShowModel(false) : setShowStatusModel(false)
        }
          
        // const setShowStatus = ()=>{
        //     console.log("setShowStatus");
        //     setShowStatusModel(false)
        // }

        let steps = "1. Go to login page/n 2. Go to login page/n 3. Go to login page/n Go to login page/n" 
        
        var withNoDigits = steps.replace(/[0-9]\./g, '').split("/n").filter(err=>err)

        //  withNoDigits = withNoDigits.split("/n").filter(err=>err)

        // console.log("steps1",withNoDigits);

        // console.log(startDate);

        const today = new Date()

        // console.log("today",today);

        const dateFormatter = (date)=>{
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
             if (month.length < 2) month = '0' + month;
             if (day.length < 2) day = '0' + day;
             let dat = [year, month, day].join('-');
             return dat

        }

        const StartDate = (e)=>{
            setValidEndDate(e)
            const date = dateFormatter(e)
            setStartDate(date)
            if(endDate.length && e ){
                setStartEndDateIsValid(true)
                setReload(true)
            }
        }

        const EndDate = (e)=>{
            setValidStartDate(e)
            const date = dateFormatter(e)
            setEndDate(date)
            if(startDate.length && e){
                setStartEndDateIsValid(true)
                setReload(true)
            }
        }

        const clearStartEndDate = ()=>{
            console.log("clearStartEndDate");
            setValidStartDate()
            setValidEndDate()
            setStartDate()
            setEndDate()
            setReload(true)
        }

        console.log(startEndDateIsValid);



    return(
        <ChakraProvider>
        <Box  w="100%" p={4} >
           <Flex  >
              <Spacer/>
              <Button bg={"#6c757d"} color="white" mr={"10px"} onClick={bugReport} >Bug Report  </Button>
              <Button bg={"#6c757d"} color="white" mr={"10px"} onClick={createNewTestCase} >Create New Test Cases </Button>
          </Flex>
        </Box>


        {/* <Box w="30%" p={4} ml={"20px"} > 
        <Flex >
        <DatePicker
            placeholderText="start date"
            value={startDate}
            onChange={StartDate}
            className="form-control"
            // minDate={validStartDate && validStartDate}
            maxDate={ validStartDate ? validStartDate : today}
        />

        <DatePicker
            placeholderText="end date"
            value={endDate}
            onChange={EndDate}            
            className="form-control"
            minDate={validEndDate && validEndDate}
            maxDate={today}
        />
        </Flex>
        </Box> */}

        <Center>
    <HStack ml={5} mb={4} w={"95%"} className="datePicker" >
      <Box w='110px' h='10' >
      <DatePicker
            placeholderText="start date"
            value={startDate}
            onChange={StartDate}
            className="form-control"
            // minDate={validStartDate && validStartDate}
            maxDate={ validStartDate ? validStartDate : today}
        />
         </Box>

        <Box w='110px' h='10'  >
        <DatePicker
            placeholderText="end date"
            value={endDate}
            onChange={EndDate}            
            className="form-control"
            minDate={validEndDate && validEndDate}
            maxDate={today}
        />
        </Box>
        <Text cursor={"pointer"} onClick={clearStartEndDate} fontFamily="Inter" > RESET </Text>
    </HStack>
    </Center>


           
         {/* <Box className="tableMovies" >    
      <DataTable   
          columns={columns}
          data={movies}
          defaultSortFieldId={1}
        //   sortIcon={<SortIcon />}
          pagination
        //   selectableRows  
        onRowClicked ={ (e)=> rowEvents(e) }
        fixedHeader
        fixedHeaderScrollHeight="80vh"
        pointerOnHover
        customStyles={customStyles}
        // striped
        highlightOnHover
        conditionalRowStyles={conditionalRowStyles}
        />  

</Box> */}

        { showModel && <Model show={showModel} testCaseData = {testCase} closeModel={setShow} reload={testCaseReload} />}

        { showStatusModel && <StatusModel show={showStatusModel} testCaseStatusData={testCaseStatus} closeModel = {setShow} reload={testCaseReload} /> }

        { loading? <Center> <Spinner size='xl' m={"120px"} /> </Center> :  <DataTable   
          columns={column}
          data={testCaseDataFromAPI}
          defaultSortFieldId={1}
          pagination
        onRowClicked ={ (e)=> rowEvents(e) }
        fixedHeader
        fixedHeaderScrollHeight="80vh"
        pointerOnHover
        customStyles={customStyles}
        highlightOnHover
        conditionalRowStyles={conditionalRowStyles}
        /> } 

    {/* <DatePicker placeholder="placeholder" border="2px solid red" /> */}


        </ChakraProvider>
    )
}


export default TestCase