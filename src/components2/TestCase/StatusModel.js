import {ChakraProvider, FormLabel, Select } from "@chakra-ui/react"
import { useState } from "react";

import {Modal, Button} from "react-bootstrap"

import axios from "axios"

import { useToast } from '@chakra-ui/react'


const StatusModel = (props)=>{

    const toast = useToast()


    const toast1 = (data,status)=>{
        return toast({
             title: status,
             description: data,
             status: status,
             position:"bottom-right",
             duration: 5000,
             isClosable: true,
           })
     }


    console.log("props",props);

    const [status, setStatus] = useState(props.testCaseStatusData.e)

    const handleShowStatus = ()=>{
        props.closeModel("StatusModel")
    }

    const statusChange = (event)=>{
        console.log(event.target.value);
        setStatus(event.target.value)
    }

    const saveStatus = async()=>{
        console.log("saveStatus",status);
        const payLoad = {
            id:props.testCaseStatusData.id,
            status:status
        }
        const updateStatus = await axios.post("http://localhost:8081/testcase/create-testcase",payLoad)
        .then(data=>data)
        .catch(err=>{
            let error = err.response ? err.response.data.message : "Something went wrong" 
            toast1(error,"error")
        })
        if(updateStatus.status === 200){
            props.reload()
            toast1(updateStatus.data.message,"success")
            props.closeModel("StatusModel")
        }
    }


    return(
        <ChakraProvider>

        <Modal show={props.show} onHide={handleShowStatus} >
        <Modal.Header closeButton>
          <Modal.Title>Update Test Case Status</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        <FormLabel htmlFor="status" > Status: </FormLabel>
            <Select  mb={"20px"} id="status" value={status} onChange={statusChange} >
            <option value='NOT TESTED'>NOT TESTED</option>
            <option value='PASS'>PASS</option>
            <option value='FAIL'>FAIL</option>
            </Select>


        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleShowStatus} >
            Close
          </Button>
          <Button variant="primary" onClick={saveStatus} >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

        </ChakraProvider>
    )
}

export default StatusModel