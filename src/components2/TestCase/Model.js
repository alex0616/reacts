import { Center, ChakraProvider, color, Flex, ListItem, OrderedList, toast, Tooltip } from "@chakra-ui/react"
import {Modal, Button} from "react-bootstrap"
import { useNavigate, useParams } from "react-router-dom";
import "./Model.css";

import {EditIcon, CloseIcon, DeleteIcon} from "@chakra-ui/icons"
import StatusModel from "./StatusModel";
import { useState } from "react";

import axios from "axios"

import { useToast } from '@chakra-ui/react'


const Model = (props)=>{

  // const [steps,setSteps] = useState(props.testCaseData.status)

    const params = useParams()

    let testCaseData = props.testCaseData 
    
    // console.log("params from model",params);

    const toast = useToast()

    const toast1 = (data,status)=>{
      return toast({
           title: status,
           description: data,
           status: status,
           position:"bottom-right",
           duration: 5000,
           isClosable: true,
         })
   }

     const handleShow = ()=>{ 
         props.closeModel("testCaseModel")
     }

     const history = useNavigate()

     const editTestCase = ()=>{
        //  console.log("editTestCase");
         history(`/editTestCase/${params.projectId}/testCaseId`,{state:{data:testCaseData}})
     }

     const deleteTestCase = async()=>{
       console.log("deleteTestCase",props.testCaseData._id);
       const deleteTestCase = await axios.delete(`http://localhost:8081/testcase/delete-testcase/${props.testCaseData.tc_id}`)
       .then(data=>data)
       .catch(err=>{
        let error = err.response ? err.response.data.message : "Something went wrong" 
        toast1(error,"error")
       })
       console.log("deleteTestCase",deleteTestCase.data.message);
       if(deleteTestCase.status===200){
         props.closeModel("testCaseModel")
         toast1(deleteTestCase.data.message,"success")
         props.reload()
       }
      //  props.setshow(false)
      //    props.reload()
     }

     let testCaseStatus = props.testCaseData.status

     const testCaseColor = testCaseStatus === "PASS" ? "testScenario testStatus text-success" : testCaseStatus === "FAIL" ? "testScenario testStatus text-danger" : "testScenario testStatus text-muted"

     console.log("props",testCaseData);

     const bugReport = (data)=>{
       console.log("bugReport",data);
       history(`/viewBugReport/${data}`)
     }

    //  let steps = ["1. Go to login page", "2. Go to logout page", "3. click the button", "4. double tap the button" ]

    // let steps = "1. Go to login page/n 2. Go to login page/n 3. Go to login page/n Go to login page/n" 
    
    let steps = props.testCaseData.steps
    
    var steps1 = steps.replace(/[0-9]\./g, '').split("\n").filter(err=>err);

    console.log("steps1", steps, steps1);

    return(
        <ChakraProvider>
      <Modal  size="lg" className="testCaseModel" show={props.show} onHide={handleShow}>
        <Modal.Header  closeButton>

          <Modal.Body className="testScenario" > <span className="headerDetails" >
             {/* TC 01  */}
             {props.testCaseData.tc_id}
             </span> </Modal.Body>

          <Modal.Body className= {testCaseColor} > {props.testCaseData.status} </Modal.Body>

          <Modal.Body className="testScenario bugReport" > {props.testCaseData.bugs.length} </Modal.Body>
          {/* {props.testCaseData.bugs.map(report=>{
            return <Modal.Body className="testScenario bugReport" onClick={bugReport.bind(null,report)} > { report} </Modal.Body>
          })} */}
        </Modal.Header>

        <Flex>
        <Modal.Body> <span className="testScenario"  > Module : </span>  
        {/* {props.testCaseData.title} */}
        {props.testCaseData.module}
        </Modal.Body>

        <Modal.Body> <span className="testScenario bugReport cursor-pointer"  > </span>
         {/* 08-02-2022 */}
        {props.testCaseData.reported_on}
          </Modal.Body>
        </Flex>

        {/* <Flex>
        <Modal.Body className="mx-auto" > <span className="testScenario"  > USERNAME : </span> Test Username   </Modal.Body>
        <Modal.Body> <span className="testScenario"  > PASSWORD : </span>  12345678 </Modal.Body>
        </Flex> */}

        {/* <Flex> */}
        <Modal.Body> <span className="testScenario"  > TEST DATA : </span>
        {/* Username: Test User Password: 12345678    */}
        {/* Username:jane,TestingPassword:janeiam */}
        {props.testCaseData.test_data}
        </Modal.Body>
        {/* </Flex> */}

        { props.testCaseData.bugs.length ? <Flex>
        <Modal.Body className="mx-auto" > <span className="testScenario"  > BUG ID's : </span>
        {/* <Tooltip label="click to view" > */}
        {props.testCaseData.bugs.map(report=>  <span className="bugs text-primary" key={Math.random()} onClick={bugReport.bind(null,report)} > {report} </span>    
         )}
         {/* </Tooltip> */}
        </Modal.Body>
        </Flex> : "" }

        {/* {props.testCaseData.bugs.map(report=>{
            return <Modal.Body className="testScenario bugReport" onClick={bugReport.bind(null,report)} > { report} </Modal.Body>
          })} */}

        {/* <Modal.Body> <span className="testScenario "  > STEPS : </span> 1.Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal! 1.Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal! 1.Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal!</Modal.Body> */}

        <Modal.Body> <span className="testScenario "  > STEPS : </span> 
        {/* {steps1} */}
        <OrderedList>
          {/* <ListItem> {steps} </ListItem> */}
          {steps1.map(data=>{
            return <ListItem key={Math.random()} > {data} </ListItem>
          })}
        </OrderedList>
        
        </Modal.Body>

        <Modal.Body> <span className="testScenario "  > TEST SCENARIO : </span> 
         {/* Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal! */}
         {props.testCaseData.test_scenario}
         </Modal.Body>

        {/* <Modal.Body className="text-center" > <span className="testScenario"  > TEST CREDENTIALS : </span> </Modal.Body> */}

        {/* <Center>
        <Flex>
        <Modal.Body className="mx-auto" > <span className="testScenario"  > USERNAME : test Username </span>  </Modal.Body>
        <Modal.Body> <span className="testScenario"  > PASSWORD : 12345678 </span> </Modal.Body>
        </Flex>
        </Center> */}


        <Modal.Body> <span className="testScenario"  > EXPECTED RESULT : </span> 
        {/* Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal!  */}
        {props.testCaseData.expected_result}
        </Modal.Body>

        <Modal.Body> <span className="testScenario"  > ACTUAL RESULT : </span> 
        {/* Woohoo, you're reading this text in a modal! Woohoo, you're reading this text in a modal!  */}
        {props.testCaseData.actual_result}
        </Modal.Body>

        <Modal.Footer>
        <Tooltip hasArrow label="Close" placement='top'>
          <Button variant="secondary" onClick={handleShow}>
            <CloseIcon/>
            {/* Close */}
          </Button>
            </Tooltip>

        <Tooltip hasArrow label="Delete" placement='top'>
          <Button variant="danger" onClick={deleteTestCase}>
            <DeleteIcon/>
            {/* Close */}
          </Button>
            </Tooltip>
            
            <Tooltip hasArrow label="Edit" placement='top'>
          <Button variant="primary" onClick={editTestCase}>
            <EditIcon/>
          </Button>
          </Tooltip>
        </Modal.Footer>
      </Modal>

        </ChakraProvider>
        
    )
}


export default Model