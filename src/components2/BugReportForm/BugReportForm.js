import { ChakraProvider, Box, Button, Center, Flex, FormControl, FormLabel, Input, Image,AspectRatio,Textarea, Select,SimpleGrid, Tooltip, GridItem, Wrap, WrapItem } from "@chakra-ui/react"
import { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import axios from "axios"

import { useToast } from '@chakra-ui/react'

const BugReportForm = (props)=>{

    const [images, setImages ] = useState([])

    const [videos, setvideos] = useState([])

    // const [file, setFile] = useState( props.bugReportFile ? props.bugReportFile : [] )

    // const [file, setFile] = useState( props.bugReportData? props.bugReportData.attachments : [] )

    const [file, setFile] = useState( [] )

    const [files, setFiles] = useState()

    const [singleFile, setSingleFile] = useState([])

    const [testCaseIdForDropDown, setTestCaseIdForDropDown] = useState([])

    const [reload, setReload] = useState(false)
    
    const [editBugReport, setEditBugReport] = useState(props.bugReportData? true : false)

    const [testCaseId, setTestCaseId] = useState(props.bugReportData&& props.bugReportData.tcId )

    const history = useNavigate()

    const {projectId} = useParams()


    useEffect(async()=>{

        await axios(`http://localhost:8081/testcase/list/${projectId}`)
        .then(data=>( setTestCaseIdForDropDown(data.data), !props.bugReportData && setTestCaseId(data.data[0])))
        .catch(err=>{
            console.log("err",err.response);
        })

    },[])

    console.log("testCaseIdForDropDown",testCaseIdForDropDown[1]);

    console.log("bugReportAttachments",props.bugReportAttachments);

    useEffect(()=>{
        if(props.bugReportData){
            console.log("props.bugReportData",props.bugReportData);
        const getAttachments = axios(`http://localhost:8081/bugreport/bug-report/${props.bugReportData.bugId}`)
        .then(data=>( setFile(data.data.attachments), console.log("data.attachments",data.data.attachments)))
        .catch(err=>{
            console.log("err",err.response);
        })
        }

        return(()=>{
            setReload(false)
        })

    },[reload])

        
        // console.log(getAttachments);
        
    

    // const filesChange = (e)=>{
    //     console.log("files",e.target.files);
    //     setFiles(e.target.files)
    //     console.log("files[0]",e.target.files[0]);
    // }

    // const formsubmit = (e)=>{
    //     e.preventDefault()
    //     console.log(files);
    //     const formData = new FormData();
    //     formData.append("attachments",files)
    //     formData.append("id","620ddcc0fdc013adb98d16c9")

    //     console.log(formData);

    //     const upload = axios.post("http://localhost:8081/bugreport/upload-file",formData,{
    //         headers:{
    //             "Content-Type":"multipart/form-data"
    //         }
    //     })
    //     .then(data=>console.log(data))
    //     .catch(err=>{
    //         console.log("err",err.response);
    //     })
    // }

    const image = (e)=>{
        console.log("image",e.target.files.length);
        // setFiles(e.target.files)
        // setSingleFile(e.target.files)
        console.log("filesConsole",e.target.files);
        // console.log("files[0]",e.target.files[0]);
        for( let i=0; i < e.target.files.length; i++ ){
            let data = {
                fileType : e.target.files[i].type,
                file : URL.createObjectURL(e.target.files[i]),
                fileName:e.target.files[i].name
            } 
            // form.append()
            // setFiles(prevData=>prevData.concat(e.target.files[i]))
            setFile(prevData=>prevData.concat(data))
            setSingleFile(prevData=>prevData.concat(e.target.files[i]))
            // if(e.target.files[i].type.includes("video")){
            //     console.log(e.target.files[i]);
            //     setvideos(prevData=> prevData.concat( URL.createObjectURL(e.target.files[i])))
            // }
            // else(
            //     setImages(prevData=>prevData.concat(URL.createObjectURL(e.target.files[i])))
            // )
        }
    }

    const cancel = async()=>{
        console.log("cancel");

        if(editBugReport && file[0]._id ){
            console.log("filefile",file);
            let attachmentsId = file.map(data=>data._id)
            console.log("attachments",attachmentsId); 
            const body = {
                fileId : attachmentsId
            }
            const deleteAttachments = await axios.post(`http://localhost:8081/bugreport/delete-file/${props.bugReportData.id}`,body)
            .then(data=> (data))
            .catch(err=>{
                console.log("err",err.response);
            })
            console.log("deleteAttachments",deleteAttachments);
            if(deleteAttachments.status=== 200 ){
            toast1(deleteAttachments.data.message,"success")
                setFile([])
                setSingleFile([])
            }
        }else{
            setFile([])
            setSingleFile([])
        }

        // setFile([])
        // setFiles([])
    }

    const removeAttachment = async(e,value)=>{

        console.log("img",e,value);

        if(editBugReport && value._id){
            console.log("bugId",props.bugReportData.id);
            console.log("attachmentId", value._id);
            const body = {
                fileId:value._id
            }
            const deleteAttachments = await axios.post(`http://localhost:8081/bugreport/delete-file/${props.bugReportData.id}`,body
            )
            .then(data=>data)
            .catch(err=>{
                console.log("err",err.response);
            })

            if(deleteAttachments.status===200){
            toast1(deleteAttachments.data.message,"success")
            let data = file.filter(file=> file.file !== e )
            setFile(data)
            console.log("deleted",deleteAttachments);
            }
        return
        }else{
            let deletedFile = file.filter(file=> file.file == e )
        console.log("deletedFile",deletedFile[0].fileName);
        // singleFile.splice(deletedFile[0].fileName,1)
        let remove = singleFile.filter(data=>data.name != deletedFile[0].fileName )
        setSingleFile(remove)
        console.log("filesAfterRemove",singleFile);

        console.log("remove",remove);


        let data = file.filter(file=> file.file !== e )
        setFile(data)
        console.log(data);
        }
        // let indexOfImage = file.map(file=>file.file ).indexOf(e)
        // files.splice(indexOfImage,1)
        // console.log("filesAfterRemove",files[indexOfImage]);
        // let indexOfImage = file.map(file=>file.file ).indexOf(e)
        // console.log("indexOfImage",indexOfImage,"indexOfsingleFile",singleFile.splice[indexOfImage]);
        // singleFile.splice(indexOfImage,1)
        // let deletedFile = file.filter(file=> file.file == e )
        // console.log("deletedFile",deletedFile[0].fileName);
        // // singleFile.splice(deletedFile[0].fileName,1)
        // let remove = singleFile.filter(data=>data.name != deletedFile[0].fileName )
        // setSingleFile(remove)
        // console.log("filesAfterRemove",singleFile);

        // console.log("remove",remove);


        // let data = file.filter(file=> file.file !== e )
        // setFile(data)
        // console.log(data);
        // props.bugReportUpdate()
    }

    console.log("file",file);

    console.log("files",files);

    console.log("singleFile",singleFile); 

    const moduleRef = useRef(props.bugReportData? props.bugReportData.module : "" )

    const bugSummaryRef = useRef(props.bugReportData? props.bugReportData.bugSummary : "" )

    // const tcIdRef = useRef(props.bugReportData? props.bugReportData.tcId : "" )

    const stepsRef = useRef(props.bugReportData? props.bugReportData.steps : "" )

    const expectedResultRef = useRef(props.bugReportData? props.bugReportData.expectedResult : "" )

    const actualResultRef = useRef(props.bugReportData? props.bugReportData.actualResult : "" )

    const bugStatusRef = useRef(props.bugReportData? props.bugReportData.bugStatus : "OPEN" )

    const bugSeverityRef = useRef(props.bugReportData? props.bugReportData.bugSeverity : "LOW" )

    const bugPriorityRef = useRef(props.bugReportData? props.bugReportData.bugPriority : "LOW" )

    const serverRef = useRef(props.bugReportData? props.bugReportData.server : "DEV" )

    const issueTypeRef = useRef(props.bugReportData? props.bugReportData.issueType : "BACKEND" )

    const lastTestedOnRef = useRef(props.bugReportData? props.bugReportData.lastTestedOn : "" )

    const reportedByRef = useRef(props.bugReportData? props.bugReportData.reportedBy : "" )

    const assignedToRef = useRef(props.bugReportData? props.bugReportData.assignedTo : "" )

    const batteryRef = useRef(props.bugReportData? props.bugReportData.battery : "" )

    const internetSpeedRef = useRef(props.bugReportData? props.bugReportData.internetSpeed : "" )

    const deviceRef = useRef(props.bugReportData? props.bugReportData.device : "" )

    const deviceVersionRef = useRef(props.bugReportData? props.bugReportData.deviceVersion : "" )

    const toast = useToast()

    const toast1 = (data,status)=>{
        console.log("data,status",data,status);
       return toast({
            title: status,
            description: data,
            status: status,
            position:"bottom-right",
            duration: 5000,
            isClosable: true,
          })
    }

    const bugReport = async(event)=>{
        event.preventDefault()

        const validationCheck = {
            "Module":moduleRef.current.value,
            "Bug Summary":bugSummaryRef.current.value,
            "Steps": stepsRef.current.value,
            "Expected Result":expectedResultRef.current.value,
            "Actual Result":actualResultRef.current.value,
            "Server":serverRef.current.value,
            "Issue Type":issueTypeRef.current.value,
            "Last Tested": lastTestedOnRef.current.value,
            "Reported By":reportedByRef.current.value,
            "Assigned To":assignedToRef.current.value,
            "Battery":batteryRef.current.value,
            "Internet Speed":internetSpeedRef.current.value,
            "Device":deviceRef.current.value,
            "Device Version":deviceVersionRef.current.value,            
        }        

        var keys = Object.keys(validationCheck);
        for (let i =0; i< keys.length; i++ ){
            console.log(validationCheck[keys[i]]);
            if(validationCheck[keys[i]].trim() === "" ){
                alert(`${keys[i]} cannot be empty`)
                return
            }
        }

            const body = {
                id:props.bugReportData ? props.bugReportData.id : "",
                module:moduleRef.current.value,
                bug_summary:bugSummaryRef.current.value,
                tc_id:testCaseId,
                // tc_id:tcIdRef.current.value,
                steps:stepsRef.current.value,
                expected_result:expectedResultRef.current.value,
                actual_result:actualResultRef.current.value,
                bug_status:bugStatusRef.current.value,
                bug_severity:bugSeverityRef.current.value,
                bug_priority:bugPriorityRef.current.value,
                server:serverRef.current.value,
                issue_type:issueTypeRef.current.value,
                last_tested:lastTestedOnRef.current.value,
                reported_by:reportedByRef.current.value,
                assigned_to:assignedToRef.current.value,
                battery:batteryRef.current.value,
                internet_speed:internetSpeedRef.current.value,
                device:deviceRef.current.value,
                device_version:deviceVersionRef.current.value,
                project: projectId
            }

            console.log(body);
            const createBugReport = await axios.post("http://localhost:8081/bugreport/create-bug-report",body)
            .then(data=>data)
            .catch(err=>{
                console.log("err",err.response);
                let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
            })
            console.log("createBugReport",createBugReport);
            console.log("singleFile.length",singleFile.length);

            {createBugReport.status ==200 && toast1(createBugReport.data.message,"success");setTestCaseId(testCaseIdForDropDown[0])
            
                    moduleRef.current.value=""
                    bugSummaryRef.current.value=""
                    stepsRef.current.value=""
                    expectedResultRef.current.value=""
                    actualResultRef.current.value=""
                    bugStatusRef.current.value="OPEN"
                    bugSeverityRef.current.value="LOW"
                    bugPriorityRef.current.value="LOW"
                    serverRef.current.value="DEV"
                    issueTypeRef.current.value="BACKEND"
                    lastTestedOnRef.current.value=""
                    reportedByRef.current.value=""
                    assignedToRef.current.value=""
                    batteryRef.current.value=""
                    internetSpeedRef.current.value=""
                    deviceRef.current.value=""
                    deviceVersionRef.current.value=""
        }

            if(createBugReport.status == 200 && singleFile.length !== 0 ){
            
                const formData = new FormData();
                for( let i=0; i < singleFile.length; i++ ){
                    formData.append("attachments",singleFile[i]) 
                }
                formData.append("id", !editBugReport ? createBugReport.data.newbugReport._id : props.bugReportData.id ) 

                const upload = await axios.post("http://localhost:8081/bugreport/upload-file",formData,{
            headers:{
                "Content-Type":"multipart/form-data"
            }
           })
          .then(data=>(data))
          .catch(err=>{
            console.log("err",err.response);
            let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
           })
           setFile([])
           setSingleFile([])
           toast1(upload.data.message,"success")
           { editBugReport && history(`/viewBugReport/${props.bugReportData.bugId}`,{replace:true}) }
            }else if(editBugReport) {
                history(`/viewBugReport/${props.bugReportData.bugId}`,{replace:true})
            }else{
                return 
            }

            





        //     if(createBugReport.status == 200 && !editBugReport ){
        //         console.log("created successfully");



        //         const formData = new FormData();
        //         for( let i=0; i < singleFile.length; i++ ){
        //             formData.append("attachments",singleFile[i]) 
        //         }
        //         // for( let i=0; i < singleFile.length; i++ ){
        //         //     formData.append("attachments",singleFile[i]) 
        //         // }

        //         // for( let i=0; i < files.length; i++ ){
        //         //     formData.append("attachments",files[i]) 
        //         // }

        //         // formData.append("attachments",files)
        //         formData.append("id",createBugReport.data.newbugReport._id) 
        //         console.log(formData);

        // const upload = await axios.post("http://localhost:8081/bugreport/upload-file",formData,{
        //     headers:{
        //         "Content-Type":"multipart/form-data"
        //     }
        // })
        // .then(data=>(data))
        // .catch(err=>{
        //     console.log("err",err.response);
        // })
        // // setReload(true)

        // console.log("upload",upload);

        //         toast1(createBugReport.data.message,"success")
        //         toast1(upload.data.message,"success")
        //         if(!props.bugReportData ){
        //             // moduleRef.current.value=""
        //             // bugSummaryRef.current.value=""
        //             // tcIdRef.current.value=""
        //             // stepsRef.current.value=""
        //             // expectedResultRef.current.value=""
        //             // actualResultRef.current.value=""
        //             // bugStatusRef.current.value="OPEN"
        //             // bugSeverityRef.current.value="LOW"
        //             // bugPriorityRef.current.value="LOW"
        //             // serverRef.current.value="DEV"
        //             // issueTypeRef.current.value="BACKEND"
        //             // lastTestedOnRef.current.value=""
        //             // reportedByRef.current.value=""
        //             // assignedToRef.current.value=""
        //             // batteryRef.current.value=""
        //             // internetSpeedRef.current.value=""
        //             // deviceRef.current.value=""
        //             // deviceVersionRef.current.value=""
        //         }else{
        //             history(`/viewBugReport/${props.bugReportData.id}`,{replace:true})
        //         }
    
        //     }
        //     else{
        //         console.log("update",props.bugReportData.id);

        //         const formData = new FormData();
        //         for( let i=0; i <= singleFile.length; i++ ){
        //             formData.append("attachments",singleFile[i]) 
        //         }
        //         // for( let i=0; i < singleFile.length; i++ ){
        //         //     formData.append("attachments",singleFile[i]) 
        //         // }

        //         // for( let i=0; i < files.length; i++ ){
        //         //     formData.append("attachments",files[i]) 
        //         // }

        //         // formData.append("attachments",files)
        //         formData.append("id",props.bugReportData.id) 
        //         console.log(formData);

        // const upload = axios.post("http://localhost:8081/bugreport/upload-file",formData,{
        //     headers:{
        //         "Content-Type":"multipart/form-data"
        //     }
        // })
        // .then(data=>( setSingleFile([]), console.log("datafor upload",data)))
        // .catch(err=>{
        //     console.log("err",err.response);
        // })
        // // setReload(true)
        //     }
           
        // console.log("bugReport",moduleRef.current.value,bugSummaryRef.current.value,tcIdRef.current.value,stepsRef.current.value,
        // expectedResultRef.current.value,actualResultRef.current.value,bugStatusRef.current.value,bugSeverityRef.current.value,
        // bugPriorityRef.current.value,serverRef.current.value,issueTypeRef.current.value,lastTestedOnRef.current.value,
        // reportedByRef.current.value,assignedToRef.current.value,batteryRef.current.value,internetSpeedRef.current.value,
        // deviceRef.current.value,deviceVersionRef.current.value);
    }

    const testCases = (e)=>{
        console.log("testCaseId",e.target.value);
        setTestCaseId(e.target.value)
    }

    return(
       <ChakraProvider>
        {/* <form onSubmit={formsubmit} > */}
        <form onSubmit={bugReport} >
            <Center>
        <FormControl width={"600px"} mt={"30px"} mb={"70px"} p={"10px"} >

        <FormLabel htmlFor="Module">Module:</FormLabel>
            <Input
              id="Module"
              type="text"
              mb={"20px"}
              autoComplete="off"
              defaultValue={props.bugReportData ? props.bugReportData.module : ""}
              ref={moduleRef}
              autoFocus
            />

            <FormLabel htmlFor="BugSummary" > Bug summary: </FormLabel>
            <Input id="BugSummary" type="text" mb={"20px"} ref={bugSummaryRef} defaultValue={props.bugReportData? props.bugReportData.bugSummary : "" } />
            
            {/* <FormLabel htmlFor="TcId" > TC ID: </FormLabel>
            <Input id="TcId" type="text" mb={"20px"} ref={tcIdRef} defaultValue={props.bugReportData? props.bugReportData.tcId : ""} /> */}
            {/* <Textarea mb={"20px"} id="TcId"/> */}

            <FormLabel htmlFor="tcId" > TC ID: </FormLabel>
            {/* <Select  mb={"20px"} id="tcId" ref={tcIdRef} value={props.bugReportData? props.bugReportData.tcId : ""} > */}
            <Select  mb={"20px"} id="tcId" value={testCaseId} onChange={testCases} >
             { testCaseIdForDropDown.map(data=>{
                 return<option value={data}>{data}</option>
             })  }        
            {/* <option value='OPEN'>OPEN</option>
            <option value='CLOSED'>CLOSED</option>
            <option value='RE-OPENED'>RE-OPENED</option> */}
            </Select>
                        
            <FormLabel htmlFor="stepsToReproduce" > Steps To Reproduce: </FormLabel>
            <Textarea mb={"20px"} id="stepsToReproduce" ref={stepsRef} defaultValue={props?.bugReportData?.steps || ""} />

            <FormLabel htmlFor="expectedResult" > Expected Result: </FormLabel>
            <Input id="expectedResult" type="text" mb={"20px"} ref={expectedResultRef} defaultValue={props.bugReportData? props.bugReportData.expectedResult : ""} />

            <FormLabel htmlFor="actualResult" > Actual Result: </FormLabel>
            <Input id="actualResult" type="text" mb={"20px"} ref={actualResultRef} defaultValue={props.bugReportData? props.bugReportData.actualResult : ""} />

            <SimpleGrid columns={2} columnGap={3} rowGap={6} w="full">

            <GridItem colSpan={1}>
            <FormLabel htmlFor="BugStatus" > BUG Status: </FormLabel>
            <Select  mb={"10px"} id="BugStatus" ref={bugStatusRef} defaultValue={props.bugReportData? props.bugReportData.bugStatus : "OPEN"} >
            <option value='OPEN'>OPEN</option>
            <option value='CLOSED'>CLOSED</option>
            <option value='REOPENED'>RE-OPENED</option>
            </Select>
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="BugSeverity" > BUG Severity: </FormLabel>
            <Select  mb={"10px"} id="BugSeverity" ref={bugSeverityRef} defaultValue={props.bugReportData? props.bugReportData.bugSeverity : "LOW"} >
            <option value='LOW'>LOW</option>
            <option value='MEDIUM'>MEDIUM</option>
            <option value='CRITICAL'>CRITICAL</option>
            </Select>
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="BugPriority" > BUG Priority: </FormLabel>
            <Select  mb={"10px"} id="BugPriority" ref={bugPriorityRef} defaultValue={props.bugReportData? props.bugReportData.bugPriority : "LOW"} >
            <option value='LOW'>LOW</option>
            <option value='MEDIUM'>MEDIUM</option>
            <option value='HIGH'>HIGH</option>
            </Select>
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="server" > Server: </FormLabel>
            <Select mb={"10px"} id="server" ref={serverRef} defaultValue={props.bugReportData? props.bugReportData.server : "DEV"} >
            <option value='DEV'>DEV</option>
            <option value='STAG'>STAG</option>
            <option value='PROD'>PROD</option>
            </Select>
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="issueType" > Issue Type: </FormLabel>
            <Select  mb={"10px"} id="issueType" ref={issueTypeRef} defaultValue={props.bugReportData? props.bugReportData.issueType : "BACKEND"} >
            <option value='UI'>UI</option>
            <option value='DEVICE SPECIFIC'>DEVICE SPECIFIC</option>
            <option value='BACKEND'>BACKEND</option>
            </Select>
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="reportedOn" > LAST TESTED ON: </FormLabel>
            <Input id="reportedOn" type="date" ref={lastTestedOnRef} defaultValue={props.bugReportData? props.bugReportData.lastTestedOn : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="reportedBy" >Reported By:</FormLabel>
            <Input id="reportedBy" type="text"  ref={reportedByRef} defaultValue={props.bugReportData? props.bugReportData.reportedBy : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="assignedTo" >Assigned To:</FormLabel>
            <Input id="assignedTo" type="text" ref={assignedToRef} defaultValue={props.bugReportData? props.bugReportData.assignedTo : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="battery" >BATTERY %:</FormLabel>
            <Input id="battery" type="number"  ref={batteryRef} defaultValue={props.bugReportData? props.bugReportData.battery : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="internetSpeed" >INTERNET SPEED:</FormLabel>
            <Input id="internetSpeed" type="number" ref={internetSpeedRef} defaultValue={props.bugReportData? props.bugReportData.internetSpeed : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="device" >DEVICE</FormLabel>
            <Input id="device" type="text"  mb={"20px"} ref={deviceRef} defaultValue={props.bugReportData? props.bugReportData.device : ""} />
            </GridItem>

            <GridItem colSpan={1}>
            <FormLabel htmlFor="deviceVersion" >DEVICE VERSION</FormLabel>
            <Input id="deviceVersion" type="text" mb={"20px"} ref={deviceVersionRef} defaultValue={props.bugReportData? props.bugReportData.deviceVersion : ""} />
            </GridItem>
            </SimpleGrid>

            <SimpleGrid  >
                {/* <Flex> */}
                <Wrap>
            {/* {images && images.map(file=>{
               return <Wrap width={"50%"} mr={"10px"} mb={"20px"} key={file} > 
                   <img src={file}  />
                     </Wrap>  
           }) }
           {videos && videos.map(file=>{
               return<Wrap width={"50%"}  mr={"10px"} key={file} >
                   <video > <source  src={`${file}#t=5`} type="video/mp4" /> </video>
                    </Wrap>
           }) } */}

           {file && file.map(file=>{
                if(file.fileType.includes("image")){
                    return <Wrap   mr={"10px"} mt={"20px"} mb={"20px"} key={file.file} cursor={"pointer"} > 
                    <Tooltip hasArrow label="Click to remove" aria-label='A tooltip' placement='top' borderRadius={"8px"} >
                    <Image src={file.file} onClick={removeAttachment.bind(null,file.file,file)} boxSize='150px' objectFit='cover' /> 
                    </Tooltip>
                     </Wrap>
                }else{
                    return <Wrap  mr={"10px"} mt={"20px"} key={`${file.file}#t=6`} cursor={"pointer"} > 
                    <Tooltip hasArrow label="Click to remove" aria-label='A tooltip' placement='top' borderRadius={"8px"} >
                    
                    <video className="videoTag" onClick={removeAttachment.bind(null,file.file,file)} style={{width:"150px", height:"150px"}} > <source src={file.file}  /> </video> 
                    
                    </Tooltip>
                    </Wrap>
                }
            })}
            </Wrap>
           {/* </Flex> */}
           { file.length ? <Button width={"auto"} onClick={cancel} mt={"20px"} > Cancel all Attachments </Button> : "" }
            </SimpleGrid>
            

            <FormLabel htmlFor="file" mt={"20px"} > ATTACHMENTS : </FormLabel>
            {/* <Input id="file" type="file" multiple mb={"20px"} onChange={filesChange} color={"white"} title=" " /> */}
            <Input id="file" type="file" accept="video/*,image/*" multiple mb={"20px"} onChange={image} color={"white"} title=" " />

            <Button type="submit" mb={"20px"} colorScheme={"blue"} >
                Submit
            </Button>

        </FormControl>
        </Center>

        </form>
       </ChakraProvider>
    )
}

export default BugReportForm