import { ChakraProvider } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { useLocation, useParams } from "react-router-dom"
import BugReportForm from "../BugReportForm/BugReportForm"

import axios from "axios"

const EditBugReport = () =>{

    const location = useLocation()

    let dataFromViewBugReportPage = location.state.data

    const {bugReportId} = useParams()

    const [reload, setReload] = useState(false)

    const [attachments, setAttachments] = useState()

    useEffect(()=>{

        const getBugReport = axios(`http://localhost:8081/bugreport/bug-report/${dataFromViewBugReportPage.bug_id}`)
        .then(data=>( setAttachments(data.data.attachments), console.log("data.data",data.data.attachments)))
        .catch(err=>{
            console.log("err",err.response);
        })

        return(()=>{
            setReload(false)
        })

    },[reload])

    const update = ()=>{
        console.log("update");
        setReload(true)
    }


    console.log("dataFromViewBugReportPage",dataFromViewBugReportPage);

    const bugReport = {
        "id":dataFromViewBugReportPage._id,
        "module":dataFromViewBugReportPage.module,
        "bugSummary":dataFromViewBugReportPage.bug_summary,
        "tcId":dataFromViewBugReportPage.tc_id,
        "bugId":dataFromViewBugReportPage.bug_id,
        "steps":dataFromViewBugReportPage.steps,
        "expectedResult":dataFromViewBugReportPage.expected_result,
        "actualResult":dataFromViewBugReportPage.actual_result,
        "bugStatus":dataFromViewBugReportPage.bug_status,
        "bugSeverity":dataFromViewBugReportPage.bug_severity,
        "bugPriority":dataFromViewBugReportPage.bug_priority,
        "server":dataFromViewBugReportPage.server,
        "issueType":dataFromViewBugReportPage.issue_type,
        "lastTestedOn":dataFromViewBugReportPage.last_tested,
        "reportedBy":dataFromViewBugReportPage.reported_by,
        "assignedTo":dataFromViewBugReportPage.assigned_to,
        "battery":dataFromViewBugReportPage.battery,
        "internetSpeed":dataFromViewBugReportPage.internet_speed,
        "device":dataFromViewBugReportPage.device,
        "deviceVersion":dataFromViewBugReportPage.device_version,
        "attachments":dataFromViewBugReportPage.attachments
    }

    const img = [{
        fileType:"image",
        // file:"https://source.unsplash.com/user/c_v_r"
        // file:"https://picsum.photos/id/1010/5184/3456"
        file:"https://testingplatformbucket.s3.ap-south-1.amazonaws.com/2013_bmw_concept_ninety-1366x768.jpg"
    },
    {
        fileType:"image",
        file:"https://picsum.photos/id/1008/5616/3744"
    },
    {
        fileType:"image",
        file:"https://picsum.photos/id/101/2621/1747"
    },
    {
        fileType:"video",
        file:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
    }
   ]

    return(
        <ChakraProvider>
            <BugReportForm bugReportData={bugReport} bugReportUpdate={update} bugReportAttachments={attachments} bugReportFile={img} ></BugReportForm>
        </ChakraProvider>
    )
}


export default EditBugReport