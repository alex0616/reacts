import { ChakraProvider } from "@chakra-ui/react"
import BugReportForm from "../BugReportForm/BugReportForm"

const AddBugReport = ()=>{

    return(
        <ChakraProvider>
            <BugReportForm></BugReportForm>
        </ChakraProvider>
    )
}


export default AddBugReport