import { ChakraProvider } from "@chakra-ui/react"
import { useLocation } from "react-router-dom"
import TestCaseForm from "../TestCaseForm/TestCaseForm"

const EditTestCase = ()=>{

    const location = useLocation()

    let testCaseData = location.state.data

    console.log("location",testCaseData);

    const testCase = {
        "_id":testCaseData._id,
        "module":testCaseData.module,
        "testScenario":testCaseData.test_scenario,
        "steps":testCaseData.steps,
        "testData":testCaseData.test_data,
        // "userName":"test user",
        // "password":"12345678",
        "expectedResult":testCaseData.expected_result,
        "actualResult":testCaseData.actual_result,
        "status":testCaseData.status
    }

    return(
        <ChakraProvider>
            <TestCaseForm testCaseData={testCase} ></TestCaseForm>
        </ChakraProvider>
    )
}

export default EditTestCase