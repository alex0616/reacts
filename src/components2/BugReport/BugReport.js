import { ChakraProvider, Box, Flex, Button, Spacer, HStack, Center, Text, Spinner } from "@chakra-ui/react"
import { useNavigate, useParams } from "react-router-dom"

import DataTable from "react-data-table-component"

import "./BugReport.css"

import movies from "../TestCase/Movies"
import { useEffect, useState } from "react"

import DatePicker from "react-datepicker";

import axios from "axios"

const BugReport = ()=>{

    const history = useNavigate()

    const {projectId} = useParams()

    const [ bugReportDataFromAPI, setBugReportDataFromAPI ] = useState([])

   const [reload, setReload] = useState(false)

    const [startDate, setStartDate] = useState("")

    const [endDate, setEndDate] = useState("")
    
    const [validStartDate, setValidStartDate] = useState()
  
    const [validEndDate, setValidEndDate] = useState()
  
    const [startEndDateIsValid, setStartEndDateIsValid] = useState(false)

    const [loading, setLoading] = useState(true)


    console.log("params",projectId);

    const bugReportFormPageHandler = ()=>{
        console.log("bugReportFormPageHandler");
        history(`/addBugReport/${projectId}`)
    }

    const testCase = ()=>{
        console.log("testCase");
        history(`/testCase/${projectId}`)
    }

    useEffect(()=>{

      const body = {
        startDate:startDate,
        endDate:endDate
        // "startDate":"2022-02-14",
        // "endDate":"2022-02-15"
    }

      axios.post(`http://localhost:8081/bugreport/all-bugreports/${projectId}`,startEndDateIsValid && body)
      .then(data=>( setBugReportDataFromAPI(data.data),setLoading(false), console.log(data.data)))
      .catch(err=>{
        console.log("err");
      })

      return(()=>{
        setReload(false)
    })

    },[reload])

    const conditionalRowStyles = [
        {
          when: row => row.bug_status !== "CLOSED" ,
          style: {
            backgroundColor: '#f5d6d6',
          },
        },
        {
          when: row => row.bug_status === "REOPNED" ,
          style: {
            backgroundColor: '#f5d6d6',
          },
        },
      ];

      const column = [
        {
          id: 1,
          name: "BG ID",
          selector: (row) => row.bug_id,
          sortable: true,
          reorder: true,
          width:"180px",
          // maxWidth:"50px"
        },
        {
          id: 2,
          name: "MODULE",
          selector: (row) => row.module,
          sortable: true,
          reorder: true,
          maxWidth:"350px"
        },
        {
          id: 3,
          name: "BUG SUMMARY",
          selector: (row) => row.bug_summary,
          sortable: true,
          reorder: true,
          maxWidth:"600px"
        },
        {
            id: 4,
            name: "BUG STATUS",
            selector: (row) => row.bug_status,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"150px"
          },
          {
            id: 5,
            name: "BUG SEVERITY",
            selector: (row) => row.bug_severity,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"165px"
          },
          {
            id: 6,
            name: "BUG PRIORITY",
            selector: (row) => row.bug_priority,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"165px"
          },
      ];

    const columns = [
        {
          id: 1,
          name: "BG ID",
          selector: (row) => row.id,
          sortable: true,
          reorder: true,
          maxWidth:"50px"
        },
        {
          id: 2,
          name: "MODULE",
          selector: (row) => row.title,
          sortable: true,
          reorder: true,
          maxWidth:"350px"
        },
        {
          id: 3,
          name: "BUG SUMMARY",
          selector: (row) => row.actors,
          sortable: true,
          reorder: true,
          maxWidth:"600px"
        },
        {
            id: 4,
            name: "BUG STATUS",
            selector: (row) => row.status,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"150px"
          },
          {
            id: 5,
            name: "BUG SEVERITY",
            selector: (row) => row.runtime,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"165px"
          },
          {
            id: 6,
            name: "BUG PRIORITY",
            selector: (row) => row.runtime,
            sortable: true,
            // right: true,
            reorder: true,
            // center:true,
            maxWidth:"165px"
          },
      ];

    const customStyles = {
        table:{
            style:{
                width:"95%",
                margin:"auto",
                border:"5px solid #6c757d",
                backgroundColor:"#6c757d"
            }
        },
        head:{
            style:{
                fontSize:"32",
                fontWeight: "bold",
            }
        },
        rows: {
            style: {
                // minHeight: '72px', // override the row height
                fontSize:"32",
            },
        },
        headCells: {
            style: {
                paddingLeft: '18px', // override the cell padding for head cells
                // paddingRight: '8px',
                // width:"50%",
            },                
        },
        cells: {
            style: {
                color:"black",
                fontFamily:"Inter",
                // background:"green"
                paddingLeft: '18px', // override the cell padding for data cells
                // paddingRight: '8px',
            },
        },
    };

    const rowEvents = (e)=>{
        console.log("rowEvents",e);
        history(`/viewBugReport/${e.bug_id}`)
    }

    const today = new Date()

    const dateFormatter = (date)=>{
      var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
       if (month.length < 2) month = '0' + month;
       if (day.length < 2) day = '0' + day;
       let dat = [year, month, day].join('-');
       return dat

  }

    const StartDate = (e)=>{
      setValidEndDate(e)
      const date = dateFormatter(e)
      setStartDate(date)
      if(endDate.length && e ){
          setStartEndDateIsValid(true)
          setReload(true)
      }

  }

  const EndDate = (e)=>{
      setValidStartDate(e)
      const date = dateFormatter(e)
      setEndDate(date)
      if(startDate.length && e){
          setStartEndDateIsValid(true)
          setReload(true)
      }
  }

  const clearStartEndDate = ()=>{
      console.log("clearStartEndDate");
      setValidStartDate()
      setValidEndDate()
      setStartDate()
      setEndDate()
      setReload(true)
  }


    return(
        <ChakraProvider>
            <Box w="100%" p={4} color="white" >
          <Flex  >
              <Spacer/>
              <Button bg={"#6c757d"} mr={"10px"} onClick={testCase} >Test Cases </Button>
               <Button bg={"#6c757d"} mr={"10px"} onClick={bugReportFormPageHandler} >Create New Bug Report </Button>
          </Flex>
      </Box>

      <Center>
    <HStack ml={5} mb={4} w={"95%"} className="datePicker" >
      <Box w='110px' h='10' >
      <DatePicker
            placeholderText="start date"
            value={startDate}
            onChange={StartDate}
            className="form-control"
            // minDate={validStartDate && validStartDate}
            maxDate={ validStartDate ? validStartDate : today}
        />
         </Box>

        <Box w='110px' h='10'  >
        <DatePicker
            placeholderText="end date"
            value={endDate}
            onChange={EndDate}            
            className="form-control"
            minDate={validEndDate && validEndDate}
            maxDate={today}
        />
        </Box>
        <Text cursor={"pointer"} onClick={clearStartEndDate} fontFamily="Inter" > RESET </Text>
    </HStack>
    </Center>

      {/* <DataTable   
          columns={columns}
          data={movies}
          defaultSortFieldId={1}
        //   sortIcon={<SortIcon />}
          pagination
        //   selectableRows  
        onRowClicked ={ (e)=> rowEvents(e) }
        fixedHeader
        fixedHeaderScrollHeight="80vh"
        pointerOnHover
        customStyles={customStyles}
        // striped
        highlightOnHover
        conditionalRowStyles={conditionalRowStyles}
        /> */}

        {loading ? <Center> <Spinner size='xl' m={"120px"} /> </Center> :

       <DataTable   
        columns={column}
        data={bugReportDataFromAPI}
        defaultSortFieldId={1}
        //   sortIcon={<SortIcon />}
        pagination
        //   selectableRows  
        onRowClicked ={ (e)=> rowEvents(e) }
        fixedHeader
        fixedHeaderScrollHeight="80vh"
        pointerOnHover
        customStyles={customStyles}
        // striped
        highlightOnHover
        conditionalRowStyles={conditionalRowStyles}
      />
        
        }

          
        </ChakraProvider>
    )
}

export default BugReport