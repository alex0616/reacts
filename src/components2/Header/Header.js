import { ChakraProvider, Box, Button, Spacer, Flex, propNames } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

import "./Header.css"

import { useToast } from '@chakra-ui/react'

const Header = (props) => {

  const toast = useToast()

  const toast1 = (data,status)=>{
    console.log("data,status",data,status);
   return toast({
        title: status,
        description: data,
        status: status,
        position:"bottom-right",
        duration: 5000,
        isClosable: true,
      })
}

  const history = useNavigate()

  const titleClickHandler = ()=>{
    console.log("titleClickHandler");
    history('/project')
  }

  const logoutHandler = ()=>{
    localStorage.removeItem("loginData")
    toast1("SignedOut Successfully","success")
    // setLoginData(null)
    props.logOut()
  }

  return (
    <ChakraProvider>
      <Box bg="#4056A1" w="100%" p={4} color="white">
        <Flex>
          <Box w="200px" h="10" pt={"7px"}>
            <span className="title" onClick={titleClickHandler} > Testing Software </span>
          </Box>
          <Spacer />
          <Box w="150px" h="10" pl={"30px"}>
            <Button type="submit" bg="#6c757d" onClick={logoutHandler} >
              LogOut
            </Button>
          </Box>
        </Flex>
      </Box>
    </ChakraProvider>
  );
};

export default Header;
