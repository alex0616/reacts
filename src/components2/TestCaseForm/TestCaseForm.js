import { Box, Button, Center, ChakraProvider, Flex, FormControl, FormLabel, Input, Textarea, Text, Select, Wrap, SimpleGrid, GridItem } from "@chakra-ui/react"
import { useRef, useState } from "react"

import {MdArrowDropDown} from "react-icons/fa"

import axios from "axios"
import { useNavigate, useParams } from "react-router-dom"

// import useToast from "@chakra-ui/toast"

import { useToast } from '@chakra-ui/react'


const TestCaseForm = (props)=>{

    const [moduleIsValid, setModuleIsValid] = useState(true)

    const [file, setFile] = useState("")
    
    const {projectId} = useParams()

    const history = useNavigate()

    const toast = useToast()

    const toast1 = (data,status)=>{
        console.log("data,status",data,status);
       return toast({
            title: status,
            description: data,
            status: status,
            position:"bottom-right",
            duration: 5000,
            isClosable: true,
          })
    }
    // console.log("props.testCaseData._id",props.testCaseData._id);

    const testCaseSubmitHandler =  async(event)=>{
        event.preventDefault()

        // if(module.trim() === ""){
        //     alert("Module can't be empty")
        //     console.log("module.trim");
        //     setModuleIsValid(false)
        //     return
        // }

        const validationCheck = {
            "Module":module,
            "Test Scenario":testScenario,
            "Steps":steps,
            "Test Data":testData,
            "Expected Result":expectedResult,
            "Status":status,
        }

        
        const body = {
            id: props.testCaseData ? props.testCaseData._id : "",
            module:module,
            test_scenario:testScenario,
            steps:steps,
            test_data:testData,
            expected_result:expectedResult,
            actual_result:actualResult,
            status:status,
            project:projectId
        }

        var keys = Object.keys(validationCheck);
        for (let i =0; i< keys.length; i++ ){
            console.log(validationCheck[keys[i]]);
            if(validationCheck[keys[i]].trim() === "" ){
                alert(`${keys[i]} cannot be empty`)
                return
            }
        }
      
      
        const payLoad = {
            "title":"title",
            "body":"ody",
            "userId":1
        }

        console.log("body",body);
        // ("https://jsonplaceholder.typicode.com/posts",payLoad)

        const createTestCase = await axios.post
        ("http://localhost:8081/testcase/create-testcase",body)
        .then(data=>data)
        .catch(err=>{
            console.log(err.response);
            let error = err.response ? err.response.data.message : "Something went wrong" 
            console.log(error);
            toast1(error,"error")
        })

        // console.log(createTestCase);

        if(createTestCase.status===200){
            toast1(createTestCase.data.message,"success")
        }
        if(props.testCaseData){
            console.log("props.testCaseData",projectId);
            history(`/testCase/${projectId}`,{replace:true})
        }
        // else{
        //     console.log(createTestCase);
        // }

        // .then(data=>console.log(data))
        // .catch(err=>{
        //     console.log("err");
        // })

        setModule("")
        setTestScenario("")
        setSteps("")
        setTestData("")
        setUserName("")
        setPassword("")
        setExpectedResult("")
        setActualResult("")
        setStatus("NOT TESTED")
        // const fd = new FormData();
        // fd.append('image', file, file.name )
        // console.log(fd);
        // moduleRef.current.value="push"
    }

    const fileHandler = (name)=>{
        console.log("name",name.target.files);
        // setFile(name.target.files)
        // console.log("name",name.target.files[0]);
        // setFile(name.target.files[0])
    }

    const [module, setModule] = useState(props.testCaseData? props.testCaseData.module : "" )

    const [testScenario, setTestScenario] = useState(props.testCaseData? props.testCaseData.testScenario : "")

    // const [steps, setSteps] = useState(props.testCaseData? props.testCaseData.steps : [])

    const [steps, setSteps] = useState(props.testCaseData? props.testCaseData.steps:"")

    const [testData, setTestData] = useState(props.testCaseData? props.testCaseData.testData : "")

    const [userName, setUserName] = useState(props.testCaseData? props.testCaseData.userName : "")

    const [password, setPassword] = useState(props.testCaseData? props.testCaseData.password : "")

    const [expectedResult, setExpectedResult ] = useState(props.testCaseData? props.testCaseData.expectedResult : "")

    const [actualResult, setActualResult] = useState(props.testCaseData? props.testCaseData.actualResult : "")

    const [status, setStatus] = useState(props.testCaseData? props.testCaseData.status : "NOT TESTED")

    console.log("props",props);

    const moduleChange = (event)=>{
        setModule(event.target.value)
        setModuleIsValid(true)
        console.log("module",event.target.value);
    }

    const testScenarioChange = (event)=>{
        setTestScenario(event.target.value)
    }

    const stepsChange = (event)=>{
        setSteps(event.target.value)
        // console.log(event.target.value);
    }

    const testDatachange = (event)=>{
        setTestData(event.target.value)
    }

    const userNameChange = (event)=>{
        setUserName(event.target.value)
    }

    const passwordChange = (event)=>{
        setPassword(event.target.value)
    }

    const expectedResultChange = (event)=>{
        setExpectedResult(event.target.value)
    }

    const actualResultChange = (event)=>{
        setActualResult(event.target.value)
    }

    const statusChange = (event)=>{
        console.log(event.target.value);
        setStatus(event.target.value)
    }

    console.log("status",steps);

    return(
        // <ChakraProvider>
            <form onSubmit={testCaseSubmitHandler} >
            <Center>
        <FormControl width={"600px"} mt={"30px"} mb={"70px"} p={"10px"} >
        <FormLabel htmlFor="Module">Module:</FormLabel>
        <Wrap mb={"20px"}>
            <Input
              id="Module"
              type="text"
            //   mb={"20px"}
              autoComplete="off"
            //   borderColor={ ! moduleIsValid && "red"}
            //   isRequired
              value={module}
            //   value={ props.testCaseData? props.testCaseData.title: module }
              onChange={moduleChange}
              autoFocus
            />
           { ! moduleIsValid && <Text color={"red"} fontSize={13} > Module can't be empty. </Text>}
            </Wrap>

            <FormLabel htmlFor="TestScenario" > Test scenario: </FormLabel>
            <Input id="TestScenario" type="text" mb={"20px"}  value={testScenario} onChange={testScenarioChange} isRequired />

            <FormLabel htmlFor="steps" > Steps: </FormLabel>
            <Textarea mb={"20px"} id="steps" value={steps} onChange={stepsChange} />
            
            {/* <SimpleGrid columns={2} columnGap={3} rowGap={6} w="full">
             <GridItem colSpan={1}>
             <FormLabel htmlFor="userName" > User Name: </FormLabel>
            <Input id="userName" type="text" mb={"20px"} value={userName} onChange={userNameChange} />
             </GridItem>
             <GridItem colSpan={1}>
             <FormLabel htmlFor="password" > password: </FormLabel>
            <Input id="password" type="text" mb={"20px"} value={password} onChange={passwordChange} />
             </GridItem>
             </SimpleGrid> */}
            
            <FormLabel htmlFor="testData" > Test Data: </FormLabel>
            <Input id="testData" type="text" mb={"20px"} value={testData} onChange={testDatachange} />

            <FormLabel htmlFor="expectedResult" > Expected Result: </FormLabel>
            <Input id="expectedResult" type="text" mb={"20px"} value={expectedResult} onChange={expectedResultChange} />

            <FormLabel htmlFor="actualResult" > Actual Result: </FormLabel>
            <Input id="actualResult" type="text" mb={"20px"} value={actualResult} onChange={actualResultChange} />

            <FormLabel htmlFor="status" > Status: </FormLabel>
            <Select  mb={"20px"} id="status"  value={status} onChange={statusChange} >
            <option value='NOT TESTED'>NOT TESTED</option>
            <option value='PASS'>PASS</option>
            <option value='FAIL'>FAIL</option>
            </Select>

            <Button type="submit" mb={"20px"} colorScheme={"blue"} >
                Submit
            </Button>

            {/* <Input type="file" multiple onChange={fileHandler} /> */}

        </FormControl>
        </Center>

        </form>

        // </ChakraProvider>
    )
}


export default TestCaseForm