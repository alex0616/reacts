import "./App.css"

import { ChakraProvider } from '@chakra-ui/react'

import { useEffect, useState } from "react"
import GoogleLogin from "react-google-login"
// import Details from "./component1/Details/Details"
// import Form from "./component1/Form/Form"
// import Header from "./component1/Header/Header"
// import DetailProvider from "./stores/DetailProvider"
import ProjectForm from "./components2/projectForm/projectForm"
import Form from "./component1/Form/Form"
import ProjectList from "./components2/projectList/ProjectList"
import Header from "./components2/Header/Header"

import { BrowserRouter as Router, Switch, Routes, Link, Route, useNavigate,Navigate } from "react-router-dom"
import ProjectLandingPage from "./components2/ProjectLandingPage/ProjectLandingPage"
import TestCase from "./components2/TestCase/TestCase"
import TestCaseForm from "./components2/TestCaseForm/TestCaseForm"
import BugReport from "./components2/BugReport/BugReport"
import BugReportForm from "./components2/BugReportForm/BugReportForm"
import SignIn from "./components2/SignIn/SignIn"
import AddTestCase from "./components2/AddTestCase/AddTestCase"
import EditTestCase from "./components2/EditTestCase/EditTestCase"
import ViewBugReport from "./components2/ViewBugReport/ViewBugReport"
import VideoPlayer from "./components2/VideoPlayer/VideoPlayer"
import AddBugReport from "./components2/AddBugReport/AddBugReport"
import EditBugReport from "./components2/EditBugReport/EditBugReport"

const Login = ({loginData, setLoginData})=>{

  // const [loginData, setLoginData] = useState(
  //   localStorage.getItem('loginData') ? JSON.parse(localStorage.getItem("loginData")) : null )
  const history = useNavigate()

  const onSuccessHandler = (googleData)=>{
    console.log("googleData",googleData,googleData.profileObj);
    localStorage.setItem("loginData",JSON.stringify(googleData.profileObj))
    setLoginData(googleData.profileObj)
  }

  const onFailureHandler = (result)=>{
    console.log("result",result);
  }

  return  <div className="googleLogin" >  
  {/* <GoogleLogin  className="googleButton"
  clientId={"722156133723-hdggnjeaa2kocs21psd26o61f4p10qbu.apps.googleusercontent.com"}
  buttonText="Please sign-in"
  onSuccess={onSuccessHandler}
  onFailure={onFailureHandler}
  cookiePolicy={"single_host_origin"}
  >
  </GoogleLogin> */}
  </div>

}

const App = ()=>{

  const [loginData, setLoginData] = useState(
    localStorage.getItem('loginData') ? JSON.parse(localStorage.getItem("loginData")) : null )

    const [createProjectForm, setProjectForm] =  useState(false)

    const history = useNavigate()

  // const onSuccessHandler = (googleData)=>{
  //   console.log("googleData",googleData,googleData.profileObj);
  //   localStorage.setItem("loginData",JSON.stringify(googleData.profileObj))
  //   setLoginData(googleData.profileObj)
  //   history('/project')
  // }

  // const onFailureHandler = (result)=>{
  //   console.log("result",result);
  // }

  const logoutHandler = ()=>{
    localStorage.removeItem("loginData")
    setLoginData(null)
  }

  // const onCreateProjectHandler = ()=>{
  //   console.log("onCreateProjectHandler");
  //   setProjectForm(true)
  // }

  return(
    <ChakraProvider>

      {/* {loginData ? 
      <Header logOut={logoutHandler} />
       :  
      <div className="googleLogin" >  
      <GoogleLogin  className="googleButton"
      clientId={"722156133723-hdggnjeaa2kocs21psd26o61f4p10qbu.apps.googleusercontent.com"}
      buttonText="Please sign-in"
      onSuccess={onSuccessHandler}
      onFailure={onFailureHandler}
      cookiePolicy={"single_host_origin"}
      >
      </GoogleLogin>
      </div>
      }       */}

    {/* <Header/> */}

    {/* <Router> */}

       { loginData ?  <>
       <Header logOut={logoutHandler} />
       <Routes>
        <Route path = "/project" element={<ProjectList/>}  ></Route>
        <Route path = "/project/:projectId" element = {<ProjectLandingPage/>} ></Route>
        <Route path = "/projectForm" element={ <ProjectForm/> }  ></Route>
        <Route path = "/projectLandingPage" element = {<ProjectLandingPage/>} ></Route>
        {/* <Route path = "/testCase" element = {<TestCase/>} ></Route> */}
        <Route path = "/testCase/:projectId" element = {<TestCase/>} ></Route>
        <Route path = "/addTestCase/:projectId" element= {<AddTestCase/>} ></Route>
        <Route path = "/editTestCase/:projectId/:testCaseId" element= {<EditTestCase/>} ></Route>
        <Route path = "/testCaseForm" element= {<TestCaseForm/>} ></Route>
        <Route path = "/bugReport/:projectId" element= {<BugReport/>} ></Route>
        <Route path = "/addBugReport/:projectId" element= {<AddBugReport/>} ></Route>
        <Route path = "/editBugReport/:projectId/:bugReportId" element= {<EditBugReport/>} ></Route>
        <Route path = "/bugReportForm" element= {<BugReportForm/>} ></Route>
        <Route path = "/viewBugReport/:bugReportId" element= {<ViewBugReport/>} ></Route>
        {/* <Route path = "/videoPlayer" element= {<VideoPlayer/>} ></Route>   */}
        <Route path="*" element={<Navigate replace to="/project" />} />
      </Routes>
      </>: 
      <Routes>
      <Route path = "/login" element={<SignIn setLoginData={setLoginData} loginData={loginData} />}  ></Route>
      <Route path="*" element={<Navigate replace to="/login" />} />
      </Routes>
       } 
      
    {/* </Router> */}
    </ChakraProvider>


    
    












      /* 
        <ChakraProvider>
      
       {loginData ? <div>
        <p> login succesful with {loginData.email} </p>
        <button onClick={logoutHandler} > LogOut </button>
        <ProjectListing/>
      </div> :  
      <div className="googleLogin" >  
      <GoogleLogin  className="googleButton"
      clientId={"722156133723-hdggnjeaa2kocs21psd26o61f4p10qbu.apps.googleusercontent.com"}
      buttonText="Please sign-in"
      onSuccess={onSuccessHandler}
      onFailure={onFailureHandler}
      cookiePolicy={"single_host_origin"}
      >
      </GoogleLogin>
      </div>
      } */
      /* <Form/> */
      /* <ProjectForm/> */


      /* <Header/>
      { !createProjectForm && <ProjectList createProject={onCreateProjectHandler} />}
      { createProjectForm && <ProjectForm/>} */
    
    // </ChakraProvider>
  )
}


export default App













// const App = ()=>{

//   const [IsDetails, setIsDetails] = useState(false)

//   const [counter, setCounter] = useState(0)

//   const setDetails = (value)=>{
//     setIsDetails(value)
//   }

//   // useEffect(()=>{
//   //   const interval = setInterval(() => {
//   //     setCounter(prevState=>prevState + 1)
//   //   }, 1000);    
//   //   return ()=> clearInterval(interval)
//   // },[])

//   return<DetailProvider>
//     <Header showdetails = {setDetails} showFormButton={IsDetails} ></Header>
//     <h2> {counter} </h2>
//     { !IsDetails && <Form details={setDetails} ></Form>}
//     { IsDetails && <Details details={setDetails} > </Details> }
//     </DetailProvider>
// }

// export default App;














// import { useState } from 'react';

// import Header from './components/Layout/Header';
// import Meals from './components/Meals/Meals';
// import Cart from './components/Cart/Cart';
// import CartProvider from './store/CartProvider';

// function App() {
//   const [cartIsShown, setCartIsShown] = useState(false);

//   const showCartHandler = () => {
//     setCartIsShown(true);
//   };

//   const hideCartHandler = () => {
//     setCartIsShown(false);
//   };

//   return (
//     <CartProvider>
//       {cartIsShown && <Cart onClose={hideCartHandler} />}
//       <Header onShowCart={showCartHandler} />
//       <main>
//         <Meals />
//       </main>
//     </CartProvider>
//   );
// }

// export default App;




















































// import "./App.css"
// import Cart from "./components1/cart/cart";

// import Header from "./components1/Header"
// import Meals from "./components1/Meal/meals";

// import { useState } from "react";
// import cartProvider from "./store/cart-provider";


// const App = ()=>{

//   const [showCart, setShowCart] = useState(false)

//   const ClickCartButton = ()=>{
//     setShowCart(true)
//   }

//   const closeCartButton = ()=>{
//     setShowCart(false)
//   }

//   return(
//     // <div className="outer" >
//         // </div>
//         // <div>
//         // <div className="Nav-bar" >
//         //   <ul>
//         //     <li> <a href="#">home</a> </li>
//         //     <li> <a href="#">About us</a> </li>
//         //     <li> <a href="#">show page</a> </li>
//         //     <li> <a href="#">contact Us</a> </li>
//         //   </ul>
//         // </div>
        

//           /* <form>

//             <label htmlFor="name" >
//               Name:
//             </label>
//             <input id="name" type="text" ></input>
//             <label htmlFor="age" >
//               age:
//             </label>
//             <input id="age" type="number" ></input>
//             <label htmlFor="number" >
//               number:
//             </label>
//             <input id="number" type="number" ></input>
//             <label htmlFor="contactnumber" >
//             contactnumber:
//             </label>
//             <input id="contactnumber"  type="number" ></input>

//             <div>
//             <input id="contactnumber1" className="contactnumber1" type="number" ></input>
//             <label htmlFor="contactnumber1" className="numbers"  >
//             contactnumber1:
//             </label>
//             <input id="contact" className="inputContact" type="number" ></input>
//             <label className="contact" >contact:</label>

//             </div>
//           </form> */


//             <cartProvider>
//           <div className="divClass" >
//             { showCart && <Cart closeCart = {closeCartButton} />}
//              {<Header showCart={ClickCartButton} ></Header>  }
//              <main>
//              <Meals/>
//              </main>
//           </div>
//           </cartProvider> 
//   )

// }


// export default App;




















// // import React, { useState,useEffect } from 'react';

// // import Login from './components/Login/Login';
// // import Home from './components/Home/Home';
// // import MainHeader from './components/MainHeader/MainHeader';
// // import AuthContext from './auth-context/AuthContext';

// // function App() {
// //   const [isLoggedIn, setIsLoggedIn] = useState(false);

// //   const checkIfTheLoginHappenedBefore = localStorage.getItem("Is_Logged")

// // useEffect(()=>{
// //   if(checkIfTheLoginHappenedBefore === "1"){
// //     setIsLoggedIn(true)
// //   }
// // },[])


// //   const loginHandler = (email, password) => {
// //     // We should of course check email and password
// //     // But it's just a dummy/ demo anyways
// //     localStorage.setItem("Is_Logged","1")
// //     setIsLoggedIn(true);
// //   };

// //   const logoutHandler = () => {
// //     console.log("came");
// //     localStorage.removeItem("Is_Logged")
// //     setIsLoggedIn(false);
// //   };

// //   return (
// //     <React.Fragment>
// //       <AuthContext.Provider value = {{isLoggedIn: isLoggedIn,onLogout : logoutHandler }}  >
// //       <MainHeader   />
// //       <main>
// //         {!isLoggedIn && <Login onLogin={loginHandler} />}
// //         {isLoggedIn && <Home onLogout={logoutHandler} />}
// //       </main>
// //       </AuthContext.Provider>
// //     </React.Fragment>
// //   );
// // }

// // export default App;











































































// // import React, { useState, useEffect, Fragment } from "react";
// // import "./App.css";
// // import Form from "./component/Form";

// // const App = () => {
// //   const [optionValue, setOptionValue] = useState([]);
// //   const [numberOfCrypto, setNumberOfCrypto] = useState(1);
// //   const [convertAmount, setConvertAmount] = useState("");
// //   const [loading, setLoading] = useState(true);
// //   const [symbol, setSymbol] = useState("BTC");
// //   //   const [crypto, setCrypto] = useState(1);

// //   //   const [numberOfAmount, setNumberOfAmount] = useState();
// //   const [currencyId, setCurrencyId] = useState(2796);
// //   const [currencySymbol, setCurrencySymbol] = useState("₹");
// //   //   const [price, setPrice] = useState(1);
// //   const [sentence, setSentence] = useState(false);
// //   //   const [amount, setAmount] = useState();

// //   let loadValue = "Loading Please wait...";

// //   const [currencyLoad, setCurrencyLoad] = useState(false);
// //   const [cryptoLoad, setCryptoLoad] = useState(false);

// //   const [showValue, setShowValue] = useState("");

// //   var url = `https://word-to-speech.herokuapp.com/getList`;

// //   useEffect(async () => {
// //     fetch(url)
// //       .then((res) => res.json())
// //       .then((response) => setOptionValue(response.data))
// //       .catch((error) => console.error("Error:", error));
// //     setLoading(true);
// //     setCurrencyLoad(true);

// //     fetch(
// //       `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${currencyId}`
// //     )
// //       .then((res) => res.json())
// //       .then(
// //         (response) => (
// //           setConvertAmount(response.data),
// //           setShowValue(response.data),
// //           setLoading(false),
// //           setCurrencyLoad(false)
// //         )
// //       )
// //       .catch((error) => console.error("Error:", error));
// //   }, []);

// //   const amountChangeHandler = (event) => {
// //     setCurrencyLoad(true);
// //     setNumberOfCrypto(event.target.value);

// //     let value = event.target.value;

// //     if (value > 0) {
// //       //   setCrypto(event.target.value);
// //       //   setLoading(true);
// //       fetch(
// //         `https://word-to-speech.herokuapp.com/conversion?amount=${event.target.value}&symbol=${symbol}&convert_id=${currencyId}`
// //       )
// //         .then((res) => res.json())
// //         .then(
// //           (response) => (
// //             setConvertAmount(response.data),
// //             // setLoading(false),
// //             setSentence(false),
// //             setCurrencyLoad(false)
// //           )
// //         )
// //         .catch((error) => console.error("Error:", error));
// //     }
// //   };

// //   const cryptoAndCurrency = (value) => {
// //     fetch(
// //       `https://word-to-speech.herokuapp.com/conversion?amount=${1}&symbol=${
// //         value.symbol ? value.symbol : symbol
// //       }&convert_id=${value.symbol ? currencyId : value}`
// //     )
// //       .then((res) => res.json())
// //       .then((response) => setShowValue(response.data))
// //       .catch((error) => console.error("Error:", error));
// //   };

// //   const changeEvent = (event) => {
// //     let value = optionValue.find((data) => data.name === event.target.value);
// //     cryptoAndCurrency(value);
// //     console.log("value", value);
// //     setSymbol(value.symbol);
// //     setConvertAmount("");
// //     setCurrencyLoad(true);
// //     setLoading(true);
// //     fetch(
// //       `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${value.symbol}&convert_id=${currencyId}`
// //     )
// //       .then((res) => res.json())
// //       .then(
// //         (response) => (
// //           setConvertAmount(response.data),
// //           setLoading(false),
// //           setSentence(false),
// //           setCurrencyLoad(false)
// //         )
// //       )
// //       .catch((error) => console.error("Error:", error));
// //   };

// //   const currencyAmountChangeHandler = async (event) => {
// //     setConvertAmount(event.target.value);
// //     // setNumberOfAmount(event.target.value);
// //     let data = event.target.value;
// //     if (data > 0) {
// //       //   setAmount(event.target.value);
// //       console.log("inside");
// //       setCryptoLoad(true);
// //       //   setLoading(true);
// //       let values;
// //       await fetch(
// //         `https://word-to-speech.herokuapp.com/conversion?amount=${1}&symbol=${symbol}&convert_id=${currencyId}`
// //       )
// //         .then((res) => res.json())
// //         .then((response) => ((values = response.data), setCryptoLoad(false)))
// //         .catch((error) => console.error("Error:", error));
// //       let value = (event.target.value / values).toFixed(20);
// //       //   setPrice(value);
// //       setNumberOfCrypto(value);
// //       //   setLoading(false);
// //       setSentence(true);
// //     }
// //   };

// //   const currencyChangeEvent = (event) => {
// //     setCurrencyLoad(true);
// //     console.log(event.target.value);
// //     let id = event.target.value.substr(0, event.target.value.indexOf(" "));
// //     setCurrencySymbol(
// //       event.target.value.substr(event.target.value.indexOf(" ") + 1)
// //     );
// //     cryptoAndCurrency(id);
// //     setCurrencyId(id);
// //     setLoading(true);
// //     fetch(
// //       `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${id}`
// //     )
// //       .then((res) => res.json())
// //       .then(
// //         (response) => (
// //           setConvertAmount(response.data),
// //           setLoading(false),
// //           setSentence(false),
// //           setCurrencyLoad(false)
// //         )
// //       )
// //       .catch((error) => console.error("Error:", error));
// //   };
// //   return (
// //     <Fragment>
// //       <Form></Form>
// //       {/* <h2 className="headingClass ">Crypto Calculator</h2>
// //       <div className="container center">
// //         <div className="row">
// //           <div className="col-md-12 d-md-flex  m-auto classPadding ">
// //             <div className="col-md-2 p-3 inputCrypto ">
// //               <input
// //                 type={`${cryptoLoad ? "text" : "number"}`}
// //                 id="cryptoName"
// //                 placeholder="Enter no. of crypto"
// //                 className="form-control"
// //                 value={`${cryptoLoad ? loadValue : numberOfCrypto}`}
// //                 onChange={amountChangeHandler}
// //               ></input>
// //             </div>
// //             <div className="col-md-3 p-3 selectCrypto ">
// //               <label className="labelStyle">select crypto</label>
// //               <select className="form-select" onChange={changeEvent}>
// //                 {optionValue.map((data) => {
// //                   return <option key={data.id}> {data.name} </option>;
// //                 })}
// //               </select>
// //             </div>
// //             <div className="col-md-2 p-3 inputCurrency">
// //               <input
// //                 type={`${currencyLoad ? "text" : "number"}`}
// //                 id="currencyName"
// //                 placeholder="Enter amount"
// //                 className="form-control"
// //                 value={`${currencyLoad ? loadValue : convertAmount}`}
// //                 onChange={currencyAmountChangeHandler}
// //               ></input>
// //             </div>
// //             <div className="col-md-3 p-3">
// //               <label className="labelStyle">select currency</label>
// //               <select className="form-select" onChange={currencyChangeEvent}>
// //                 <option value="2796 ₹"> Indian Rupee (₹INR) </option>
// //                 <option value="2781 $"> United States Dollar ($USD) </option>
// //                 <option value="2787 ¥"> Chinese Yuan (¥CNY) </option>
// //                 <option value="2790 €"> Euro (€EUR) </option>
// //                 <option value="2813 د.إ">
// //                   United Arab Emirates Dirham (د.إAED)
// //                 </option>
// //               </select>
// //             </div>
// //           </div>
// //         </div>
// //       </div>
// //       <h3 className="value">
// //         {loading
// //           ? loadValue
// //           : `${1} ${symbol} = ${currencySymbol} ${showValue}`}
// //       </h3> */}
// //       {/* <h2 className="headingClass">Crypto Calculator</h2>

// //       <div className="divClass">
// //         <div>
// //           <label htmlFor="cryptoName" className="cryptoName">
// //             Select Crypto
// //           </label>
// //           <label htmlFor="currencyName" className="currencyName">
// //             Select Currency
// //           </label>
// //         </div>
// //         <input
// //         type={`${cryptoLoad? "text" :"number" }`}
// //           id="cryptoName"
// //           placeholder="Enter no. of crypto"
// //           value={`${cryptoLoad? loadValue : numberOfCrypto }`}
// //           onChange={amountChangeHandler}
// //         ></input>
// //         <select className="wrapper" onChange={changeEvent}>
// //           {optionValue.map((data) => {
// //             return <option key = {data.id} > {data.name} </option>;
// //           })}
// //         </select>
// //         <input
// //         type={ `${currencyLoad? "text" :"number" }` }

// //           id="currencyName"
// //           placeholder="Enter amount"
// //           value = {`${ currencyLoad? loadValue : convertAmount }`}

// //           onChange={currencyAmountChangeHandler}
// //         ></input>
// //         <select className="selectClass" onChange={currencyChangeEvent}>
// //           <option value="2796 ₹"> Indian Rupee (₹INR) </option>
// //           <option value="2781 $"> United States Dollar ($USD) </option>
// //           <option value="2787 ¥"> Chinese Yuan (¥CNY) </option>
// //           <option value="2790 €"> Euro (€EUR) </option>
// //           <option value="2813 د.إ">United Arab Emirates Dirham (د.إAED)</option>
// //         </select>
// //         <div>
// //           <h2 className="value">
// //               {loading? loadValue : `${1} ${symbol} = ${currencySymbol} ${showValue}` } */}
// //       {/* {loading
// //               ? "Loading Please wait..."
// //               : `${
// //                   sentence
// //                     ? `${amount} ${currencySymbol}  = ${price} ${symbol} `
// //                     : `${crypto} ${symbol} cost = ${currencySymbol} ${convertAmount} `
// //                 }`} */}
// //       {/* </h2>
// //         </div>
// //       </div> */}
// //     </Fragment>
// //   );
// //   // return (
// //   //   <Fragment>
// //   //     <h2> Crypto calculator </h2>

// //   //     <div className="divClass">
// //   //       {/* <label className="cryptoLabel">selectcrypto </label>
// //   //       <label className="currencyLabel">selectcurrency</label> */}
// //   //       <form>
// //   //         <input type="number" className="cryptoInput"></input>
// //   //       <label className="cryptoLabel">selectcrypto </label>
// //   //         <select className="cryptoSelect">
// //   //           <option>Bitcoin</option>
// //   //         </select>
// //   //         <input type="number" className="currencyInput"></input>
// //   //       <label className="currencyLabel">selectcurrency</label>
// //   //         <select className="currencySelect">
// //   //           <option>BitcoinBitcoinBitcoinBitcoinBitcoinBitcoin</option>
// //   //           <option>BitcoinBitcoinBitcoinBitcoinBitcoinBitcoin</option>
// //   //         </select>
// //   //       </form>
// //   //     </div>
// //   //   </Fragment>
// //   // );
// // };

// // export default App;
