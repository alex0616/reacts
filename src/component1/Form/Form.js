import { useContext, useState } from "react";
import Input from "../UI/Input";
import Label from "../UI/Label";
import classes from "./Form.module.css";
import axios from "axios";

import DetailContext from "../../stores/DetailContext";

const Form = (props) => {
  const [nameState, setNameState] = useState("");
  const [ageState, setageState] = useState("");
  const [phoneNumberState, setphoneNumberState] = useState("");
  const [msg, setMsg] = useState(false);

  const detailCtx = useContext(DetailContext);

  const nameHandler = (name) => {
    console.log(name);
    setNameState(name);
    setMsg(false);
  };

  const ageHandler = (age) => {
    setageState(age);
    setMsg(false);
  };

  const phoneNumberHandler = (phoneNumber) => {
    setphoneNumberState(phoneNumber);
    setMsg(false);
  };

  const handler = (value) => {
    detailCtx.addDetails(value);
  };

  const submitHandler = async (event) => {
    event.preventDefault();
    if (
      nameState.trim().length === 0 ||
      ageState.trim().length === 0 ||
      phoneNumberState.trim().length === 0
    ) {
      setMsg(true);
      return;
    }
    handler({
      defaultKey:Math.random(),
      name: nameState,
      age: ageState,
      phoneNumber: phoneNumberState,
    });
    // const requestOptions = {
    //   method: "POST",
    //   headers: { "Content-Type": "application/json" },
    //   body: { name: nameState, age: ageState, phoneNumber: phoneNumberState },  
    // };

    // const value = await axios
    //   .post("http://localhost:8081/formData",
    //   {
    //     name: nameState,
    //     age: ageState,
    //     phoneNumber: phoneNumberState,
    //   }
    //   )
    //   .then((data) => console.log("data from API", data.data))
    //   .catch((err) => console.log(err));
    props.details(true);
  };

  return (
    <form className={classes.form1} onSubmit={submitHandler}>
      <h4 className={classes.fillHeading}> please fill the form </h4>
      <div className={classes.labelInputDiv}>
        <Label htmlFor={"nameInput"} name={"name:"}></Label>
        <Input
          id={"nameInput"}
          type={"text"}
          value={nameState}
          eventHandler={nameHandler}
        ></Input>
        <Label htmlFor={"ageInput"} name={"age:"}></Label>
        <Input
          id={"ageInput"}
          type={"number"}
          value={ageState}
          eventHandler={ageHandler}
        ></Input>
        <Label htmlFor={"phoneNumberInput"} name={"phone number:"}></Label>
        <Input
          id={"phoneNumberInput"}
          type={"number"}
          value={phoneNumberState}
          eventHandler={phoneNumberHandler}
        ></Input>
        <button className={classes.submitButton}> submit </button>
        {msg && <h5> please enter all the fields </h5>}
      </div>
    </form>
  );
};

export default Form;
