import { useContext } from "react";
import DetailContext from "../../stores/DetailContext";
import classes from "./Details.module.css";
import DetailsList from "./DetailsList/DetailsList";

const Details = (props) => {
  const detailCtx = useContext(DetailContext);
  console.log("detail", detailCtx);

  const detailsProvider = detailCtx.detail.map((item) => (
    <DetailsList
      key={Math.random()}
      defaultKey={item.defaultKey}
      name={item.name}
      age={item.age}
      phoneNumber={item.phoneNumber}
      phoneNumbe={item.phoneNumber}
    ></DetailsList>
  ));

  console.log("detailsProvider",detailsProvider.length);

  console.log("detailCtx",detailCtx);

  const closeHandler = () => {
    props.details(false);
  };

  return (
    <div className={classes.detailsDiv}>
      {detailsProvider}
      {!detailsProvider.length && <p className={classes.emptyHeader} >Empty Please fill the form</p>}
      <button className={classes.closeButton} onClick={closeHandler}>Close</button>
    </div>
  );
};

export default Details;
