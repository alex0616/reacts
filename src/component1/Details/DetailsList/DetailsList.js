import classes from "./DetailsList.module.css";
import DetailContext from "../../../stores/DetailContext";
import { useContext } from "react";

const DetailsList = (props) => {

  const detailsCtx = useContext(DetailContext)

  const removeHandler = ()=>{
    console.log("removeHandler",props.defaultKey);
    detailsCtx.removeDetails(props.defaultKey)

  }


  return (
    <ul>
      <li className={classes.name}>{props.defaultKey}</li>
      <li className={classes.name}>{props.name}</li>
      <li className={classes.age}>{props.age}</li>
      <li className={classes.phoneNumber}>{props.phoneNumber}</li>
      <button className={classes.removeButton} onClick={removeHandler} > Remove </button>
    </ul>
  );
};

export default DetailsList;
