import classes from "./Header.module.css";
import DetailContext from "../../stores/DetailContext";
import { useContext } from "react";

const Header = (props) => {

  const detailCtx = useContext(DetailContext)

  const showDetailsHandler = ()=>{
    props.showdetails(true)
  }

  const showFormHandler = ()=>{
    props.showdetails(false)
  }

  console.log("header",detailCtx.detail.length);

  return (
    <div className={classes.divHeader}>
      <h2 className={classes.header} > Welcome </h2>
      { !props.showFormButton && <button className={classes.formPageButton} onClick={showDetailsHandler} >Show details</button>}
      { props.showFormButton && <button className={classes.formPageButton} onClick={showFormHandler} >Show form</button>}
      {/* <p className={classes.details} > Enter the details </p> */}
      <p className={classes.headerDetailsCount} > Number of Details Entered: {detailCtx.detail.length} </p>
    </div>
  );
};

export default Header;
