import { Fragment, useRef } from "react";
import classes from "./Input.module.css";


const Input = (props) => {
  const handler = (event) => {
    props.eventHandler(event.target.value);
  };

  return (
    <input
      id={props.id}
      type={props.type}
      className={classes.input}
      value={props.value}
      onChange={handler}
    ></input>
  );
};

export default Input;
