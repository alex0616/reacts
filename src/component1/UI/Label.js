import classes from "./Label.module.css";

const Label = (props) => {
  return (
    <label htmlFor={props.htmlFor} className={classes.formLabel}>
      {props.name}
    </label>
  );
};

export default Label;
