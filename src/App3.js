import "./App.css";
import React, { useState, useEffect } from "react";
// import axios from "axios";

// import FormData from "./component/FormData";

const App = () => {
  const [optionValue, setOptionValue] = useState([]);
  const [numberOfCrypto, setNumberOfCrypto] = useState(1);
  const [convertAmount, setConvertAmount] = useState("");
  const [loading, setLoading] = useState(true);
  const [symbol, setSymbol] = useState("BTC");
  const [crypto, setCrypto] = useState(1);

  const [numberOfAmount, setNumberOfAmount] = useState();
  const [currencyId, setCurrencyId] = useState(2796);
  const [currencySymbol, setCurrencySymbol] = useState("₹");
  const [price, setPrice] = useState(0);
  const [sentence, setSentence] = useState(false);

  var url = `https://word-to-speech.herokuapp.com/getList`;

  useEffect(async () => {
    await fetch(url)
      .then((res) => res.json())
      .then((response) => setOptionValue(response.data))
      .catch((error) => console.error("Error:", error));
    setLoading(true);

    await fetch(
      `http://localhost:8081/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${currencyId}`
    )
      .then((res) => res.json())
      .then((response) => (setConvertAmount(response.data), setLoading(false)))
      .catch((error) => console.error("Error:", error));
  }, []);

  const amountChangeHandler = (event) => {
    setNumberOfCrypto(event.target.value);

    let value = event.target.value;

    if (value > 0) {
      setCrypto(event.target.value);
      setLoading(true);
      fetch(
        `http://localhost:8081/conversion?amount=${event.target.value}&symbol=${symbol}&convert_id=${currencyId}`
      )
        .then((res) => res.json())
        .then(
          (response) => (
            setConvertAmount(response.data),
            setLoading(false),
            setSentence(false)
          )
        )
        .catch((error) => console.error("Error:", error));
    }
  };

  const changeEvent = (event) => {
    let value = optionValue.find((data) => data.name === event.target.value);
    console.log("value", value);
    setSymbol(value.symbol);
    setConvertAmount("");
    setLoading(true);
    fetch(
      `http://localhost:8081/conversion?amount=${numberOfCrypto}&symbol=${value.symbol}&convert_id=${currencyId}`
    )
      .then((res) => res.json())
      .then(
        (response) => (
          setConvertAmount(response.data), setLoading(false), setSentence(false)
        )
      )
      .catch((error) => console.error("Error:", error));
  };

  const currencyAmountChangeHandler = async (event) => {
    setNumberOfAmount(event.target.value);
    let data = event.target.value;
    if (data > 0) {
      setLoading(true);
      let values;
      await fetch(
        `http://localhost:8081/conversion?amount=${1}&symbol=${symbol}&convert_id=${currencyId}`
      )
        .then((res) => res.json())
        .then((response) => (values = response.data))
        .catch((error) => console.error("Error:", error));
      let value = (event.target.value / values).toFixed(20);
      setPrice(value);
      setLoading(false);
      setSentence(true);
    }
  };

  const currencyChangeEvent = (event) => {
    console.log(event.target.value);
    let id = event.target.value.substr(0, event.target.value.indexOf(" "));
    setCurrencySymbol(
      event.target.value.substr(event.target.value.indexOf(" ") + 1)
    );
    setCurrencyId(id);
    setLoading(true);
    fetch(
      `http://localhost:8081/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${id}`
    )
      .then((res) => res.json())
      .then(
        (response) => (
          setConvertAmount(response.data), setLoading(false), setSentence(false)
        )
      )
      .catch((error) => console.error("Error:", error));
  };

  return (
    // <body>
    <div className="selected">
      <h2> Let's go and let's do it.. </h2>
      <form className="formClass">
        <h2> Cryptocurrency Converter Calculator.. </h2>

        <div className="amount">
          <input
            type="number"
            min="1"
            placeholder="Enter no of crypto's to be converted"
            className="inputClass"
            value={numberOfCrypto}
            onChange={amountChangeHandler}
          />
          <input
            type="number"
            min="1"
            placeholder="Enter the amount"
            className="inputClass"
            value={numberOfAmount}
            onChange={currencyAmountChangeHandler}
          />
        </div>

        <select className="selectClass" onChange={changeEvent}>
          {optionValue.map((data) => {
            return <option> {data.name} </option>;
          })}
        </select>

        <select className="selectClass" onChange={currencyChangeEvent}>
          <option value="2796 ₹"> Indian Rupee (₹INR) </option>
          <option value="2781 $"> United States Dollar ($USD) </option>
          <option value="2787 ¥"> Chinese Yuan (¥CNY) </option>
          <option value="2790 €"> Euro (€EUR) </option>
          <option value="2813 د.إ">United Arab Emirates Dirham (د.إAED)</option>
        </select>

        <div>
          <h2>
            {loading
              ? "Loading Please wait..."
              : `${
                  sentence
                    ? `${numberOfAmount} ${currencySymbol}  = ${price} ${symbol} `
                    : `${crypto} ${symbol} cost = ${currencySymbol} ${convertAmount} `
                }`}
          </h2>
        </div>
      </form>
    </div>
    // </body>
  );

  // const [form, setForm] = useState(true);
  // const [ name, setName ] = useState("");
  // const [ age, setAge ] = useState("");
  // const [ sampleData, setSampleData ] = useState([{id:"1",name:"samplename", age:"sample age"}])

  // const formpage = (event)=>{
  //   event.preventDefault()
  //   setForm(false)
  // }

  // const cancelHandler = ()=>{
  //   setForm(true)
  // }

  // const nameHandler = (event)=>{
  //   setName(event.target.value)
  //   console.log(event.target.value);
  // }

  // const ageHandler = (event)=>{
  //   setAge(event.target.value)
  // }

  // var values = {}

  // const submitHandler = (event)=>{
  //   event.preventDefault()
  //    setSampleData([{name:name,age:age}])
  //   setName('')
  //   setAge('')
  //   setForm(true)
  //   console.log(values);
  // }

  // if(form){
  //   return(
  //     <div className="onlyButton" >
  //       {sampleData.map(data=>  <h2>  Name: {data.name}  age: {data.age} </h2> )}
  //       <form onSubmit={formpage} >
  //         <button type = "submit" > Add form </button>
  //       </form>
  //       </div>
  //   )
  // }

  // return (
  //   <div className="App">
  //     <h2> Let's go and let's do it.. </h2>

  //     <form className="formClass" onSubmit = {submitHandler} >
  //       <div>
  //         <label> Name: </label>
  //         <input type="text" value = {name} onChange={nameHandler} ></input>
  //       </div>
  //       <div>
  //         <label> age: </label>
  //         <input type="text" value={age} onChange={ageHandler} ></input>
  //       </div>
  //       <div>
  //         <button type="submit"> Submit </button>
  //         <button type = "submit" onClick={cancelHandler} > cancel </button>
  //       </div>
  //     </form>

  //   </div>

  // <div className="App">
  //   <header className="App-header">
  //     <img src={logo} className="App-logo" alt="logo" />
  //     <p>
  //       Edit <code>src/App.js</code> and save to reload.
  //     </p>
  //     <a
  //       className="App-link"
  //       href="https://reactjs.org"
  //       target="_blank"
  //       rel="noopener noreferrer"
  //     >
  //       Learn React
  //     </a>
  //   </header>
  // </div>
  // );
};

export default App;
