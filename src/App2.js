import { useState } from "react";
import AddUser from "./Components/AddUser";
import DisplayUser from "./Components/DisplayUser";
import Error from "./Components/Error";


const App = () => {

    const[userValues, setUserValues] = useState([])

    const getValues = (name,age)=>{
        setUserValues((prevState)=>{
            return[...prevState, {name:name, age:age,id: Math.random().toString()  }]
        })
    }


  return (
    <div>
      <h2> Let's go and Let's do it.. </h2>
      <AddUser onClick = {getValues} ></AddUser>
      <DisplayUser onClick = {userValues} ></DisplayUser>
      {/* <Error></Error> */}
    </div>
  );
};

export default App;
