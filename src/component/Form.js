import React, { useState, useEffect, Fragment } from "react";

import "./Form.css";

const Form = () => {
  const [optionValue, setOptionValue] = useState([]);
  const [numberOfCrypto, setNumberOfCrypto] = useState(1);
  const [convertAmount, setConvertAmount] = useState("");
  const [loading, setLoading] = useState(true);
  const [symbol, setSymbol] = useState("BTC");

  const [currencyId, setCurrencyId] = useState(2796);
  const [currencySymbol, setCurrencySymbol] = useState("₹");

  let loadValue = "Loading Please wait...";

  const [currencyLoad, setCurrencyLoad] = useState(false);
  const [cryptoLoad, setCryptoLoad] = useState(false);

  const [showValue, setShowValue] = useState("");

  var url = `https://word-to-speech.herokuapp.com/getList`;

  useEffect(async () => {
    fetch(url)
      .then((res) => res.json())
      .then((response) => setOptionValue(response.data))
      .catch((error) => console.error("Error:", error));
    setLoading(true);
    setCurrencyLoad(true);

    fetch(
      `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${currencyId}`
    )
      .then((res) => res.json())
      .then(
        (response) => (
          setConvertAmount(response.data),
          setShowValue(response.data),
          setLoading(false),
          setCurrencyLoad(false)
        )
      )
      .catch((error) => console.error("Error:", error));
  }, []);

  const amountChangeHandler = (event) => {
    setCurrencyLoad(true);
    setNumberOfCrypto(event.target.value);

    let value = event.target.value;

    if (value > 0) {
      fetch(
        `https://word-to-speech.herokuapp.com/conversion?amount=${event.target.value}&symbol=${symbol}&convert_id=${currencyId}`
      )
        .then((res) => res.json())
        .then(
          (response) => (
            setConvertAmount(response.data), setCurrencyLoad(false)
          )
        )
        .catch((error) => console.error("Error:", error));
    }
  };

  const cryptoAndCurrency = (value) => {
    fetch(
      `https://word-to-speech.herokuapp.com/conversion?amount=${1}&symbol=${
        value.symbol ? value.symbol : symbol
      }&convert_id=${value.symbol ? currencyId : value}`
    )
      .then((res) => res.json())
      .then((response) => setShowValue(response.data))
      .catch((error) => console.error("Error:", error));
  };

  const changeEvent = (event) => {
    let value = optionValue.find((data) => data.name === event.target.value);
    cryptoAndCurrency(value);
    console.log("value", value);
    setSymbol(value.symbol);
    setConvertAmount("");
    setCurrencyLoad(true);
    setLoading(true);
    fetch(
      `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${value.symbol}&convert_id=${currencyId}`
    )
      .then((res) => res.json())
      .then(
        (response) => (
          setConvertAmount(response.data),
          setLoading(false),
          setCurrencyLoad(false)
        )
      )
      .catch((error) => console.error("Error:", error));
  };

  const currencyAmountChangeHandler = async (event) => {
    setConvertAmount(event.target.value);
    let data = event.target.value;
    if (data > 0) {
      console.log("inside");
      setCryptoLoad(true);
      let values;
      await fetch(
        `https://word-to-speech.herokuapp.com/conversion?amount=${1}&symbol=${symbol}&convert_id=${currencyId}`
      )
        .then((res) => res.json())
        .then((response) => ((values = response.data), setCryptoLoad(false)))
        .catch((error) => console.error("Error:", error));
      let value = (event.target.value / values).toFixed(20);
      setNumberOfCrypto(value);
    }
  };

  const currencyChangeEvent = (event) => {
    setCurrencyLoad(true);
    console.log(event.target.value);
    let id = event.target.value.substr(0, event.target.value.indexOf(" "));
    setCurrencySymbol(
      event.target.value.substr(event.target.value.indexOf(" ") + 1)
    );
    cryptoAndCurrency(id);
    setCurrencyId(id);
    setLoading(true);
    fetch(
      `https://word-to-speech.herokuapp.com/conversion?amount=${numberOfCrypto}&symbol=${symbol}&convert_id=${id}`
    )
      .then((res) => res.json())
      .then(
        (response) => (
          setConvertAmount(response.data),
          setLoading(false),
          setCurrencyLoad(false)
        )
      )
      .catch((error) => console.error("Error:", error));
  };
  return (
    <Fragment>
      <h2 className="headingClass ">Crypto Calculator</h2>
      <div className="container center">
        <div className="row">
          <div className="col-md-12 d-md-flex  m-auto classPadding ">
            <div className="col-md-2 p-3 inputCrypto ">
              <input
                type={`${cryptoLoad ? "text" : "number"}`}
                id="cryptoName"
                placeholder="Enter no. of crypto"
                className="form-control"
                value={`${cryptoLoad ? loadValue : numberOfCrypto}`}
                onChange={amountChangeHandler}
              ></input>
            </div>
            <div className="col-md-3 p-3 selectCrypto ">
              <label className="labelStyle">select crypto</label>
              <select className="form-select" onChange={changeEvent}>
                {optionValue.map((data) => {
                  return <option key={data.id}> {data.name} </option>;
                })}
              </select>
            </div>
            <div className="col-md-2 p-3 inputCurrency">
              <input
                type={`${currencyLoad ? "text" : "number"}`}
                id="currencyName"
                placeholder="Enter amount"
                className="form-control"
                value={`${currencyLoad ? loadValue : convertAmount}`}
                onChange={currencyAmountChangeHandler}
              ></input>
            </div>
            <div className="col-md-3 p-3">
              <label className="labelStyle">select currency</label>
              <select className="form-select" onChange={currencyChangeEvent}>
                <option value="2796 ₹"> Indian Rupee (₹INR) </option>
                <option value="2781 $"> United States Dollar ($USD) </option>
                <option value="2787 ¥"> Chinese Yuan (¥CNY) </option>
                <option value="2790 €"> Euro (€EUR) </option>
                <option value="2813 د.إ">
                  United Arab Emirates Dirham (د.إAED)
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <h3 className="value">
        {loading
          ? loadValue
          : `${1} ${symbol} = ${currencySymbol} ${showValue}`}
      </h3>
    </Fragment>
  );
};

export default Form;
