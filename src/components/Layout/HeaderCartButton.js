import { useContext, useEffect, useState } from 'react';

import CartIcon from '../Cart/CartIcon';
import CartContext from '../../store/Cart-context';
import classes from './HeaderCartButton.module.css';

const HeaderCartButton = (props) => {
  const cartCtx = useContext(CartContext);
  const {items} = cartCtx;
  // const [bump,setBump] = useState(false)
  const [btnIsHighlighted, setBtnIsHighlighted] = useState(false);

  const cartButton = `${classes.button} ${ btnIsHighlighted? classes.bump: ""}`

  useEffect(() => {
    if (items.length === 0) {
      return;
    }
    setBtnIsHighlighted(true);

    const timer = setTimeout(() => {
      setBtnIsHighlighted(false);
    }, 300);

    return () => {
      clearTimeout(timer);
    };
  }, [items]);

  // useEffect(()=>{
  //   if(items.length ===  0){
  //     return
  //   }
  //   setBump(true)
  //   const timer = setTimeout(()=>{
  //     setBump(false)
  //   },300)
  //   return()=>{
  //     clearTimeout(timer)
  //   }
  // },[items])

  const numberOfCartItems = cartCtx.items.reduce((curNumber, item) => {
    return curNumber + item.amount;
  }, 0);

  return (
    <button className={cartButton} onClick={props.onClick}>
      <span className={classes.icon}>
        <CartIcon />
      </span>
      <span>Your Cart</span>
      <span className={classes.badge}>{numberOfCartItems}</span>
    </button>
  );
};

export default HeaderCartButton;