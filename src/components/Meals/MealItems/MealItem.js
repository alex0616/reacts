import MealItemForm from './MealItemForm';
import classes from './MealItem.module.css';
import CartContext from '../../../store/Cart-context';
import { useContext } from 'react';


const MealItem = (props) => {
  const price = `$${props.price.toFixed(2)}`;

  const cartCtx = useContext(CartContext)

  const addCart = (enteredValue)=>{
    cartCtx.addItem({
      id:props.id,
      name:props.name,
      amount:enteredValue,
      price:props.price
    })
  }

  return (
    <li className={classes.meal}>
      <div>
        <h3>{props.name}</h3>
        <div className={classes.description}>{props.description}</div>
        <div className={classes.price}>{price}</div>
      </div>
      <div>
        <MealItemForm onAdd = {addCart} id={props.id} />
      </div>
    </li>
  );
};

export default MealItem;