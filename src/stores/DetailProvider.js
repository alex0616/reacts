import { useState } from "react";
import DetailContext from "./DetailContext";

const DetailProvider = (props) => {
  const [detailsList, setDetailsList] = useState({});
  const [previousDetailsList, setpreviousDetailsList] = useState([]);


  const addDetailsHandler = (values) => {
    console.log("detailsProvider", values);
    setDetailsList(values);
    setpreviousDetailsList((prevState)=>{
      return [...prevState,values]
    })
  };

  const removeDetailsHandler = (values)=>{
    setpreviousDetailsList( prevState=> prevState.filter(items=>items.defaultKey !== values) )
  }

  const details = {
    detail:
      previousDetailsList
    ,
    details: [
      {
        name: detailsList.name,
        age: detailsList.age,
        phoneNumber: detailsList.phoneNumber,
      },
    ],
    addDetails: addDetailsHandler,
    removeDetails:removeDetailsHandler
  };

  return (
    <div>
      <DetailContext.Provider value={details}>
        {props.children}
      </DetailContext.Provider>
    </div>
  );
};

export default DetailProvider;
