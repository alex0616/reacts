import React from "react"

const DetailContext = React.createContext ({
    detail:"",
    details:[],
    addDetails:(details)=>{},
    removeDetails:(details)=>{}
})

export default DetailContext

