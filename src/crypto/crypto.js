import React, { useState, useEffect, Fragment } from "react";

import "./crypto.css";

const Crypto = (props) => {
    console.log("propsprops",props.cryptoList);
    
  return (
    <Fragment>
      <h2 className="headingClass ">Crypto Calculator</h2>
      <div className="container center">
        <div className="row">
          <div className="col-md-12 d-md-flex  m-auto classPadding ">
            <div className="col-md-2 p-3 inputCrypto ">
              <input
                type="number"
                id="cryptoName"
                placeholder="Enter no. of crypto"
                className="form-control"
              ></input>
            </div>
            <div className="col-md-3 p-3 selectCrypto ">
              <label className="labelStyle">select crypto</label>
              <select className="form-select">
                {props.cryptoList.map((data) => {
                  return <option key={data.id}> {data.name} </option>;
                })}
              </select>
            </div>
            <div className="col-md-2 p-3 inputCurrency">
              <input
                type="number"
                id="currencyName"
                placeholder="Enter amount"
                className="form-control"
              ></input>
            </div>
            <div className="col-md-3 p-3">
              <label className="labelStyle">select currency</label>
              <select className="form-select">
                <option value="2796 ₹"> Indian Rupee (₹INR) </option>
                <option value="2781 $"> United States Dollar ($USD) </option>
                <option value="2787 ¥"> Chinese Yuan (¥CNY) </option>
                <option value="2790 €"> Euro (€EUR) </option>
                <option value="2813 د.إ">
                  United Arab Emirates Dirham (د.إAED)
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
      {/* <h3 className="value">
        {loading
          ? loadValue
          : `${1} ${symbol} = ${currencySymbol} ${showValue}`}
      </h3> */}
    </Fragment>
  );
};

export default Crypto;
