import React, { useState, useEffect, useReducer } from "react";

import Card from "../UI/Card/Card";
import classes from "./Login.module.css";
import Button from "../UI/Button/Button";

const emailFunction = (state, action) => {
  console.log("actionaction", action);

  if (action.type === "User_Email") {
    return { val: action.emailValue, isValid: action.emailValue.includes("@") };
  }

  if (action.type === "email_Blur") {
    console.log("state", state);
    return { val: state.val, isValid: state.val.includes("@") };
  }

  return { val: "", isValid: false };
};

const passwordFunction = (state, action) => {
  console.log("passwordactionaction", action);

  if (action.type === "user_Password") {
    return {
      val: action.passwordValue,
      isValid: action.passwordValue.trim().length > 6,
    };
  }

  if (action.type === "password_Blur") {
    console.log("statestate", state);
    return { val: state.val, isValid: state.val.trim().length > 6 };
  }
  return { type: "", isValid: false };
};

const Login = (props) => {
  // const [enteredEmail, setEnteredEmail] = useState('');
  // const [emailIsValid, setEmailIsValid] = useState();
  // const [enteredPassword, setEnteredPassword] = useState("");
  // const [passwordIsValid, setPasswordIsValid] = useState();
  const [formIsValid, setFormIsValid] = useState(false);

  const [email, dispatchEmail] = useReducer(emailFunction, {
    val: "",
    isValid: undefined,
  });

  const [password, dispatchPassword] = useReducer(passwordFunction, {
    val: "",
    isValid: null,
  });

  useEffect(() => {
    const identifier = setTimeout(() => {
      console.log("key stroke");
      setFormIsValid(email.isValid && password.isValid);
    }, 500);
    return () => {
      // console.log("cleanup");
      clearTimeout(identifier);
    };
  }, [email.isValid, password.isValid]);

  const emailChangeHandler = (event) => {
    // setEnteredEmail(event.target.value);
    dispatchEmail({ type: "User_Email", emailValue: event.target.value });
    // setFormIsValid(event.target.value.includes("@") && password.isValid )
  };

  const passwordChangeHandler = (event) => {
    // setEnteredPassword(event.target.value);
    dispatchPassword({
      type: "user_Password",
      passwordValue: event.target.value,
    });
    // setFormIsValid(email.isValid && event.target.value.trim().length>6 )
  };

  const validateEmailHandler = () => {
    // setEmailIsValid(enteredEmail.includes('@'));
    dispatchEmail({ type: "email_Blur" });
  };

  const validatePasswordHandler = () => {
    // setPasswordIsValid(password.val.trim().length > 6);
    dispatchPassword({ type: "password_Blur" });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    props.onLogin(email.val, password.val);
  };

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <div
          className={`${classes.control} ${
            email.isValid === false ? classes.invalid : ""
          }`}
        >
          <label htmlFor="email">E-Mail</label>
          <input
            type="email"
            id="email"
            value={email.val}
            onChange={emailChangeHandler}
            onBlur={validateEmailHandler}
          />
        </div>
        <div
          className={`${classes.control} ${
            password.isValid === false ? classes.invalid : ""
          }`}
        >
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={password.val}
            onChange={passwordChangeHandler}
            onBlur={validatePasswordHandler}
          />
        </div>
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn} disabled={!formIsValid}>
            Login
          </Button>
        </div>
      </form>
    </Card>
  );
};

export default Login;
