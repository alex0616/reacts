import { useState } from "react/cjs/react.development";
import "./AddUser.css";

import Error from "./Error";

const AddUser = (props) => {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [error, setError] = useState();

  const nameListener = (event) => {
    setName(event.target.value);
  };

  const ageListener = (event) => {
    setAge(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (name.trim().length === 0) {
      setError({
        title: "Invalid Input",
        header: "please enter the name before submit",
      });
      return;
    }
    if (age.trim().length === 0) {
        setError({
          title: "Invalid Input",
          header: "please enter the age before submit",
        });
        return;
      }
    if (age < 0 || age >99 ) {
      setError({
        title: "Invalid Input",
        header: `Age ${age} is not acceptable please enter valid age.`,
      });
      return;
    }

    props.onClick(name, age);
    setName('')
    setAge('')
  };

  return (
    <div>
      {error && <Error onClick={error}></Error>}
      <div className="addUserClass">
        <form onSubmit={submitHandler}>
          <label htmlFor="name"> Name: </label>
          <input
            type="text"
            id="name"
            placeholder="Please enter the Name..."
            value={name}
            onChange={nameListener}
          />

          <label htmlFor="age"> Age: </label>
          <input
            type="number"
            id="age"
            placeholder="Please enter the Age..."
            value={age}
            onChange={ageListener}
          />
          <button type="submit" className="buttonClass">
            Add Details
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddUser;
