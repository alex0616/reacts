import "./Error.css";

const Error = (props) => {
  console.log(props);

  return (
    <div className="backdrop">
      <form className="errorClass">
        <header> {props.onClick.title} </header>
        <h2> {props.onClick.header} </h2>
        <button className="errorbuttonClass">Close</button>
      </form>
    </div>
  );
};

export default Error;
