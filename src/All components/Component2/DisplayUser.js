import "./DisplayUser.css";

const DisplayUser = (props) => {
  console.log("propsprops", props.onClick);

  return (
    <div className="users" >
      <form className="userList">
        {props.onClick.map((data) => (
          <h4 key={data.id}>
            Name: {data.name} age: {data.age}
          </h4>
        ))}
      </form>
    </div>
  );
};

export default DisplayUser;
