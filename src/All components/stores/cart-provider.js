import { useReducer } from 'react';

import CartContext from './cart-content';

const defaultCartState = {
  items: [],
  totalAmount: 0
};

const cartReducer = (state, action) => {
  if (action.type === 'ADD') {
    const updatedItems = state.items.concat(action.item);
    const updatedTotalAmount = state.totalAmount + action.item.price * action.item.amount;
    console.log("action",action);
    return {
      items: updatedItems,
      totalAmount: updatedTotalAmount
    };
  }
  return defaultCartState;
};

const CartProvider = (props) => {

  const [cartState, dispatchCartState] = useReducer(cartReducer, defaultCartState)


  const addItemToCartHandler = (item) => {
      console.log("item",item);
    dispatchCartState({type: 'ADD', item: item});
  };

  const removeItemFromCartHandler = (id) => {
    dispatchCartState({type: 'REMOVE', id: id});
  };

  const cartContext = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemToCartHandler,
    removeItem: removeItemFromCartHandler,
  };

  return (
    <CartContext.Provider value={cartContext}>
      {props.children}
    </CartContext.Provider>
  );
};

export default CartProvider;













// import CartContext from "./cart-content";

// import { useReducer } from "react";

// const defaultCartState = {
//     items:[],
//     totalAmount:0
// }

// const cartReducer = (state,action)=>{

//     if(action.type === "ADD"){
//         const updatedItems = state.items.concat(action.item);
//         const updatedTotalAmount = state.totalAmount + state.item.price * state.item.amount;
//         return{
//             items:updatedItems,
//             totalAmount:updatedTotalAmount
//         }
//     }
//     return defaultCartState;
// }


// const cartProvider = (props)=>{

//     const [cartState, dispatchCartState] = useReducer(cartReducer, defaultCartState)

//     const addItemToTheCart = (item)=>{
//         dispatchCartState({type:"ADD", item:item })
//     }

//     const removeItemToTheCart = (id)=>{
//         dispatchCartState({type:"REMOVE"})
//     }

//     const cartContext = {
//         items:[],
//         totalAmount:0,
//         addItem:addItemToTheCart ,
//         removeItem:removeItemToTheCart ,
//     }

//     return <CartContext.Provider value={cartContext} >
//         {props.children}
//     </CartContext.Provider>

// }


// export default cartProvider;