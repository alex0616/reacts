
import { Fragment } from "react";

import classes from "./button.module.css"



const Button = (props)=>{
    return(
    <Fragment>
        <button className={classes.button} onClick={props.yourCartbutton} > 
        <span> {props.buttonName} </span>
        <span>{props.numberOfItems} </span>
         </button>
    </Fragment>
)
}


export default Button