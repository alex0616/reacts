import { useRef, useContext } from "react";
import Button from "../../button/button";
import classes from "./MealItem.module.css"

import cartContext from "../../../store/cart-content";


const MealItem = (props)=>{

  const amountInputRef = useRef()

  const cartCtx = useContext(cartContext)
  
  const submitHandler = (event)=>{
    event.preventDefault()
    const enteredAmount = amountInputRef.current.value;
    const enteredAmountNumber = +enteredAmount;
    
    console.log("preventDefault",enteredAmountNumber);
    
    cartCtx.addItem({
      id:props.id,
      name:props.name,
      amount:enteredAmountNumber,
      price:props.price
    })
  }

    return(
      <form onSubmit={submitHandler} >
      <div className={classes.mainDiv} >
        <li className={classes.meal}>
        <div>
          <h4>{props.name}</h4>
          <div className={classes.description}>{props.description}</div>
          <div className={classes.price}>{props.price}</div>
          <label className={classes.label} >Amount</label>
          <input type="number" className={classes.amountInput} defaultValue="1" ref={amountInputRef} ></input>
          <button className={classes.addButton} > + Add  </button>
        </div>
        <div>
          {/* <MealItemForm id={props.id} /> */}
        </div>
      </li>
      </div>
      </form>
    )

}


export default MealItem