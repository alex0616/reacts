import { Fragment, useContext } from "react";
import classes from "./Header.module.css";
import meals from "../meals.jpg";
import Button from "./button/button";
import cartContext from "../store/cart-content";

const Header = (props) => {

  const cartCtx = useContext(cartContext)

  const numberofItemsInCart = cartCtx.items.reduce((crtNumber, item)=>{
    console.log("item.amount",cartCtx.items);
    return crtNumber + item.amount
  } ,0 )

  console.log("numberofItemsInCartnumberofItemsInCart",numberofItemsInCart,cartCtx.items);

  return (
    <Fragment>
      <header className={classes.headingNav}>
        <h2> React Nav </h2>
        <Button
          buttonName="My Cart "
          numberOfItems = {numberofItemsInCart}
          yourCartbutton={props.showCart}
        ></Button>
      </header>
      <div className={classes.divImage}>
        <img src={meals} className={classes.mealImage}></img>
      </div>
    </Fragment>
  );
};

export default Header;
