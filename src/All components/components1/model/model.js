import { Fragment } from "react";
import reactDom from "react-dom"; 
import classes from "./model.module.css"


const Backdrop = (props) => {
    return <div className={classes.backdrop} onClick={props.onClose} />;
  };
  

const ModalOverlay = (props) => {
    return (
      <div className={classes.modal}>
        <div className={classes.content}>{props.children}</div>
      </div>
    );
  };



const Model = (props)=>{



    const portels = document.getElementById("overLays")

    return(
    <Fragment>
        {reactDom.createPortal(<Backdrop onClose = {props.clickBackdrop} />,portels)}
        {reactDom.createPortal(<ModalOverlay>{props.children}</ModalOverlay>,portels)}
    </Fragment>
    )


}


export default Model;